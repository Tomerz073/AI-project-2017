from __future__ import print_function
from pynput.keyboard import Key, Controller
import os
import listener
import neat
import visualize
import glob
import time

#for threading
import ThreadWithReturnValue as thrd

#defines
CHECKPOINT_PER_GENS = 5
GENS = 1000
DATA_LEN = 50
ACT_BTN = 0.99999
START_FITNESS = 320
L = 0
U = 1
R = 2

socket = listener.establishConnection()

#for keyboard
keyboard = Controller()

#sets up static variable
def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

#deletes useless checkoints in directory
def deleteCheckpoints():

    files = []
    for file in glob.glob("*"):
        if "neat-checkpoint-" in file:
            files.append(file)

    files.sort(key=os.path.getmtime)

    for file in files[0:len(files)-1]:
        if "neat-checkpoint" in file:
            os.remove(file)
            files.remove(file)

    os.rename(files[0], "reserved checkpoints/" + files[0])

#loads most recent checkpoint
def loadCheckpoint(checkpoint_path):
    return neat.Checkpointer.restore_checkpoint(checkpoint_path)

#function for evolution of genomes, send and gets information from the game
@static_vars(counter=0)
def eval_genomes(genomes, config):
    pop_counter = 0
    for genome_id, genome in genomes:
        #sends genome and generation data
        sendData = "GENOME ID:" + str(genome_id) + " "  + "POP NO:" + str(pop_counter) + " " + "GEN:" + str(eval_genomes.counter)
        listener.sendData(socket, sendData)
        #sets fitness and ceates the net
        genome.fitness = START_FITNESS
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        data = thrdTCP(socket) # tuple of args for foo
        #for loop for rest of data
        while data != "RESET" and data != "EXIT" and socket != None:
            #splits data into list of block numbers and fitness
            dataArr = map(float, data.split())
            #analyzes the data and fintness from the list
            area = dataArr[0:DATA_LEN]
            fitness = dataArr[DATA_LEN]
            #sets fitness
            genome.fitness = fitness
            #create threads
            ai_thread = thrd.ThreadWithReturnValue(target=thrdAi, args=(net, area)) # tuple of args for foo
            tcp_thread = thrd.ThreadWithReturnValue(target=thrdTCP, args=(socket,)) # tuple of args for foo
            #starts threads
            ai_thread.start()
            tcp_thread.start()
            #joins threads to main procces
            ai_thread.join()
            data = tcp_thread.join()  # get the return value from your function.

        pressButtons([0.5,0.5,0.5])#deactivates buttons, new genome is tested
        pop_counter += 1#count for next pop in gen

        #if user sent exit closes
        if data == "EXIT":
            listener.closeSocket(socket)
            break

    #delets useless prev checkpoint
    #if (eval_genomes.counter % (CHECKPOINT_PER_GENS - 1) == 0):
    #    deleteCheckpoints()

    eval_genomes.counter += 1#static counter for gens

#funtion for ai thread, gets output from net and presses buttons
def thrdAi(net ,area):
    output = net.activate(area)
    pressButtons(output)

#function for ai thread, returns data from game
def thrdTCP(socket):
    try:
        return listener.getData(socket)
    except:
        socket.close()
        return None
        raise

#function for pressing buttons
def pressButtons(output):
    if output[L] >= ACT_BTN:
        keyboard.press(Key.left)
    if output[U] >= ACT_BTN:
        keyboard.press(Key.up)
    if output[R] >= ACT_BTN:
        keyboard.press(Key.right)

    if output[L] < ACT_BTN and keyboard.pressed(Key.left):
        keyboard.release(Key.left)
    if output[U] < ACT_BTN and keyboard.pressed(Key.up):
        keyboard.release(Key.up)
    if output[R] < ACT_BTN and keyboard.pressed(Key.right):
        keyboard.release(Key.right)

def run(config_file, checkpoint_path):
    # Load configuration.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_file)

    # Create the population, which is the top-level object for a NEAT run.
    p = None
    if checkpoint_path != None:
        p = loadCheckpoint(checkpoint_path)
    else:
        p = neat.Population(config)

    #Add a stdout reporter to show progress in the terminal.
    #p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(CHECKPOINT_PER_GENS))

    # Run for up to 1000 generations.
    winner = p.run(eval_genomes, GENS)
    #sends exit
    listener.sendData(socket, "EXIT")
    listener.closeSocket(socket)

    # Display the winning genome.
    print('\nBest genome:\n{!s}'.format(winner))

    #visualize part
    node_names = {0:'LEFT', 1: 'UP', 2:'RIGHT'}
    visualize.draw_net(config, winner, True, node_names=node_names)
    visualize.plot_stats(stats, ylog=False, view=True)
    visualize.plot_species(stats, view=True)

#main of function
def main():

    # Determine path to configuration file. This path manipulation is
    # here so that the script will run successfully regardless of the
    # current working directory

    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config-feedforward')
    checkpoint_path = menu()
    while True:
        try:
            run(config_path, checkpoint_path)
        except:
            print("error with connecting to socket")

        #deletes useless checkpoints:
        deleteCheckpoints()
        user_choice = raw_input("Do you want to continue? [y/n]:")
        if user_choice == 'y':
            checkpoint_path = menu()

#returns checkpoint path from user choice
def menu():
    user_choice = '0'
    while user_choice != '1' and user_choice != '2' and user_choice != '3':
        print ("press 1 to start learning from scrap")
        print ("press 2 to continue from existing checkpoint")
        user_choice = raw_input("enter your choice:")

    checkpoint_path = None

    if user_choice == '1':
        checkpoint_path =  None
    elif user_choice == '2':
        checkpoint_path =  raw_input("enter the checkpoint path:")

    print ("GO BACK TO RUNI/O TO SEE HE AI PROGRESS!!!")
    time.sleep(5)

    return checkpoint_path

main()