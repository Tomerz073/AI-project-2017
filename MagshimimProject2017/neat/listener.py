import socket
from socket import error as socket_error
import sys
import errno

def establishConnection():
	flag = 0

	while flag == 0:
		# Create a TCP/IP socket
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		# Connect the socket to the port where the server is listening
		server_address = ('127.0.0.1', 27015)
		print >>sys.stderr, 'connecting to %s port %s' % server_address

		try:
			sock.connect(server_address)
			flag = 1
		except socket_error as serr:
			if serr.errno != errno.ECONNREFUSED:
				raise serr
			flag = 0

	return sock

def getData(sock):
	try:
		# Wait for a connection
		data = sock.recv(1024)
		return data
	except:
		raise

def sendData(sock, data):
	try:
		# Wait for a connection
		sock.send(data)
	except:
		raise

def closeSocket(sock):
	print >>sys.stderr, 'closing socket'
	try:
		sock.close()
	except:
		raise