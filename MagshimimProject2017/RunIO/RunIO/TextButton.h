#include "defines.h"

#pragma once

class TextButton
{
private:
	ALLEGRO_FONT* _font;
	int _x;
	int _y;
	int _defX;
	int _defY;
	ALLEGRO_COLOR _mainColor;
	ALLEGRO_COLOR _secondaryColor;
	int _mode;
	char _textNonConst[128];
	const char* _text;

public:
	TextButton(int x, int y, ALLEGRO_COLOR mainColor, ALLEGRO_COLOR secondaryColor, int mode, const char* text, const char* path, int size);
	~TextButton();
	void drawButton();
	void changeColor(ALLEGRO_COLOR newMainColor, ALLEGRO_COLOR newSecondaryColor);
	bool equalColors(ALLEGRO_COLOR newMainColor, ALLEGRO_COLOR newSecondaryColor);
	void moveButton(float posXchange);
	void setText(const char* text);
	void setXpos(int xPos);
	void setYpos(int yPos);
	int getXpos();
	int getYpos();
	string getText();
};
