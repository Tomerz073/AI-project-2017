#include "GameCredits.h"
#include "TextButton.h"
#include "maploader.h"
#include "GameLevel.h"
#include "Helper.h"
#include "TextButton.cpp" //*WORKS LIKE THAT DO NOT CHANGE .CPP - LINKER IS WIERD*

#pragma once

/*
c`tor of menu
INPUT:
const char* path - the path of the menu map that is being loaded
OUTPUT:
none
*/
GameCredits::GameCredits(const char* path)
{
	this->_x = MOVE_BUTTON_CONST;
	this->_path = path;

	this->loadButtons();//function that sets and loads the buttons

	this->_creditsStatus = NULL;
}

/*
loads menu buttons and titles
INPUT:
none
OUTPUT:
none
*/
void GameCredits::loadButtons()
{
	//load first second third from file code


	//end code

	//setting up colors
	ALLEGRO_COLOR COLOR_FONT_BLUE = al_map_rgb(44, 117, 255);
	ALLEGRO_COLOR COLOR_FONT_DARK_BLUE = al_map_rgb(22, 59, 127);
	ALLEGRO_COLOR COLOR_FONT_WHITE = al_map_rgb(255, 255, 255);
	ALLEGRO_COLOR COLOR_FONT_GRAY = al_map_rgb(185, 185, 185);
	ALLEGRO_COLOR COLOR_FONT_DARK_GRAY = al_map_rgb(70, 70, 70);
	ALLEGRO_COLOR COLOR_FONT_BLACK = al_map_rgb(0, 0, 0);

	//adds the buttons to the button vector in the menu class
	this->_titles.push_back(new TextButton(MENU_TITLE_X, MENU_TITLE_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Credits", FONT_PATH, TITLE_SIZE));
	this->_titles.push_back(new TextButton(16, MENU_BUTTON2_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Tomer Tzipori           Raz Wolpe", FONT_PATH, MENU_BUTTON_SIZE));
	this->_titles.push_back(new TextButton(32, MENU_BUTTON3_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Game C++ Library:", FONT_PATH, MENU_BUTTON_SIZE - 4));
	this->_titles.push_back(new TextButton(SCREEN_WIDTH - 32, MENU_BUTTON3_Y, COLOR_FONT_BLACK, COLOR_FONT_GRAY, ALLEGRO_ALIGN_RIGHT, "Allegro 5.0", FONT_PATH, MENU_BUTTON_SIZE - 4));
	this->_titles.push_back(new TextButton(32, MENU_BUTTON4_Y - 24, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Python AI Library:", FONT_PATH, MENU_BUTTON_SIZE - 4));
	this->_titles.push_back(new TextButton(SCREEN_WIDTH - 32, MENU_BUTTON4_Y - 24, COLOR_FONT_BLACK, COLOR_FONT_GRAY, ALLEGRO_ALIGN_RIGHT, "neat-python", FONT_PATH, MENU_BUTTON_SIZE - 4));
	this->_titles.push_back(new TextButton(32, MENU_BUTTON4_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Inspiration Source:", FONT_PATH, MENU_BUTTON_SIZE - 4));
	this->_titles.push_back(new TextButton(SCREEN_WIDTH - 32, MENU_BUTTON4_Y, COLOR_FONT_BLACK, COLOR_FONT_GRAY, ALLEGRO_ALIGN_RIGHT, "'MarI/O' by SethBling", FONT_PATH, MENU_BUTTON_SIZE - 4));

	this->_titles.push_back(new TextButton(SCREEN_WIDTH / 2, SCREEN_HEIGHT - 16, COLOR_FONT_BLACK, COLOR_FONT_GRAY, ALLEGRO_ALIGN_CENTER, ">> Press ESC to exit <<", FONT_PATH, MENU_BUTTON_SIZE - 4));
	this->_titles.push_back(new TextButton(32, MENU_BUTTON5_Y, COLOR_FONT_BLACK, COLOR_FONT_GRAY, ALLEGRO_ALIGN_LEFT, "This is a 2D Super Mario-based game, with", FONT_PATH, MENU_BUTTON_SIZE - 4));
	this->_titles.push_back(new TextButton(32, MENU_BUTTON5_Y + 16, COLOR_FONT_BLACK, COLOR_FONT_GRAY, ALLEGRO_ALIGN_LEFT, "option to run an AI bot that solves this", FONT_PATH, MENU_BUTTON_SIZE - 4));
	this->_titles.push_back(new TextButton(32, MENU_BUTTON5_Y + 32, COLOR_FONT_BLACK, COLOR_FONT_GRAY, ALLEGRO_ALIGN_LEFT, "game as fast and as good as possible.", FONT_PATH, MENU_BUTTON_SIZE - 4));
	this->_titles.push_back(new TextButton(32, MENU_BUTTON5_Y + 48, COLOR_FONT_BLACK, COLOR_FONT_GRAY, ALLEGRO_ALIGN_LEFT, "We hope you'll enjoy out project!", FONT_PATH, MENU_BUTTON_SIZE - 4));


	//this->_buttons.push_back(new TextButton(MENU_BUTTON_X, MENU_BUTTON2_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "AI", FONT_PATH, MENU_BUTTON_SIZE));
	//this->_buttons.push_back(new TextButton(MENU_BUTTON_X, MENU_BUTTON3_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "ScoreBoard", FONT_PATH, MENU_BUTTON_SIZE));
	//this->_buttons.push_back(new TextButton(MENU_BUTTON_X, MENU_BUTTON4_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Credits", FONT_PATH, MENU_BUTTON_SIZE));
	//this->_buttons.push_back(new TextButton(MENU_BUTTON_X, MENU_BUTTON5_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Exit", FONT_PATH, MENU_BUTTON_SIZE));
}

/*
loads the menu map
INPUT:
none
OUTPUT:
none
*/
void GameCredits::loadCredits()
{
	//using this->_path and levelLoader.h to load the tiles
	this->_tileSet = loadMap(this->_path, this->_map);
}

/*
d`tor of menu
INPUT:
none
OUTPUT:
none
*/
GameCredits::~GameCredits()
{
	//removes allocated memory in buttons
	deleteButtons(this->_buttons);
	deleteButtons(this->_titles);

	//delets camera memory

	//deletes map memory
	this->_map.clear();
	al_destroy_bitmap(this->_tileSet);
}

/*
handles the run of the game
INPUT:
screen - source screen of the program
OUTPUT:
returns the menu status - the next mode(PLAY, AI, SCOREBOARD, CREDITS) or EXIT
*/
int GameCredits::handleGameCredits(ALLEGRO_DISPLAY* display)
{
	//sets up parameters
	bool done = false;
	bool draw = true;

	//setting allegro variables 
	ALLEGRO_EVENT events;//contains single event 
	ALLEGRO_EVENT_QUEUE* eventQueue = al_create_event_queue();//contains a queue of events
	ALLEGRO_TIMER* creditTimer = al_create_timer(1.0 / FPS); //sets up the local game handle timer (60 FPS)
	ALLEGRO_KEYBOARD_STATE keyState; //contains keyboard event 

	al_register_event_source(eventQueue, al_get_keyboard_event_source());//sets up keyboard
	al_register_event_source(eventQueue, al_get_timer_event_source(creditTimer));//sets up timers 
	al_register_event_source(eventQueue, al_get_display_event_source(display));//sets up screen

	drawMap(this->_tileSet, this->_map);//draws the map

	drawVectorButtons(this->_buttons); //draws the buttons
	drawVectorButtons(this->_titles);
	al_draw_bitmap(al_load_bitmap(MAGSHIMIM_LOGO_PATH), 164, 50, 0);

	al_flip_display();

	al_start_timer(creditTimer);//starts timer

	while (!done)//while the user didnt exit
	{
		//waits for events
		al_wait_for_event(eventQueue, &events);

		//if the user exit the program
		if (events.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			this->_creditsStatus = EXIT;//exit from the game
			done = true;//current run ends
			break;//faster exit 
		}
		//else if timer event
		else if (events.type == ALLEGRO_EVENT_TIMER)
		{

			//gets keyboard state
			al_get_keyboard_state(&keyState);
			done = this->handleKeyboard(&keyState);//handles keyboard input
		}
	}

	//destorys allocated memory with allegro5 functions
	al_destroy_event_queue(eventQueue);
	al_destroy_timer(creditTimer);

	return this->_creditsStatus;
}


/*
handles the input from the user
INPUT:
ALLEGRO_KEYBOARD_STATE* keyState - pointer to the keyboard state
OUTPUT:
returns true if the input mean end game run, false if else
*/
int GameCredits::handleKeyboard(ALLEGRO_KEYBOARD_STATE* keyState)
{
	int done = false;//the return value
	static int escHold = RELEASE;

	if (al_key_down(keyState, ALLEGRO_KEY_ESCAPE) && escHold == RELEASE)//if user pressed exit
	{
		escHold = HOLD;
	}
	else if (!al_key_down(keyState, ALLEGRO_KEY_ESCAPE) && escHold == HOLD)//if user released exit exit
	{
		escHold = RELEASE;
		this->_creditsStatus = MENU;//exit from the game
		done = true;//game run ends 
	}

	return done;//returns game loop state value

	
}