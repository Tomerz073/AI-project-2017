#include "defines.h"

#pragma once

using namespace std;

class TextButton;

class GameHighScore
{
private:

	int _x;

	const char* _path;

	int _hscoreStatus;

	vector<TextButton*> _buttons;
	vector<TextButton*> _titles;

	vector<vector<int>> _map;
	ALLEGRO_BITMAP* _tileSet;

public:
	GameHighScore(const char* path);
	~GameHighScore();
	void loadHighScore();
	int handleGameHighScore(ALLEGRO_DISPLAY* display);
	int handleKeyboard(ALLEGRO_KEYBOARD_STATE* keyState);
	void loadButtons();
};