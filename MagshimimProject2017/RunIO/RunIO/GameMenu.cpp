#include "GameMenu.h"
#include "TextButton.h"
#include "maploader.h"
#include "GameLevel.h"
#include "Helper.h"
#include "TextButton.cpp" //*WORKS LIKE THAT DO NOT CHANGE .CPP - LINKER IS WIERD*

#pragma once

/*
c`tor of menu
INPUT:
	const char* path - the path of the menu map that is being loaded
OUTPUT:
	none
*/
GameMenu::GameMenu(const char* path)
{
	this->_x = MOVE_BUTTON_CONST;
	this->_path = path;
	this->_menuStatus = NULL;

	this->loadButtons();//function that sets and loads the buttons

	//camera optional
	this->_camera = new ALLEGRO_TRANSFORM();

	//sets up values in camera pos arr
	this->_cameraPosition[CAMERA_POS_X_INDEX] = 0;
	this->_cameraPosition[CAMERA_POS_Y_INDEX] = 0;
}

/*
loads menu buttons and titles
INPUT:
	none
OUTPUT:
	none
*/
void GameMenu::loadButtons()
{	
	
	//setting up colors
	ALLEGRO_COLOR COLOR_FONT_BLUE = al_map_rgb(44, 117, 255);
	ALLEGRO_COLOR COLOR_FONT_DARK_BLUE = al_map_rgb(22, 59, 127);
	
	//adds the buttons to the button vector in the menu class
	this->_titles.push_back(new TextButton(MENU_TITLE_X, MENU_TITLE_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "RunI/O", FONT_PATH, TITLE_SIZE));
	this->_buttons.push_back(new TextButton(MENU_BUTTON_X, MENU_BUTTON1_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Play", FONT_PATH, MENU_BUTTON_SIZE));
	this->_buttons.push_back(new TextButton(MENU_BUTTON_X, MENU_BUTTON2_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "AI", FONT_PATH, MENU_BUTTON_SIZE));
	this->_buttons.push_back(new TextButton(MENU_BUTTON_X, MENU_BUTTON3_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "ScoreBoard", FONT_PATH, MENU_BUTTON_SIZE));
	this->_buttons.push_back(new TextButton(MENU_BUTTON_X, MENU_BUTTON4_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Credits", FONT_PATH, MENU_BUTTON_SIZE));
	this->_buttons.push_back(new TextButton(MENU_BUTTON_X, MENU_BUTTON5_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Exit", FONT_PATH, MENU_BUTTON_SIZE));
}

/*
loads the menu map
INPUT:
	none
OUTPUT:
	none
*/
void GameMenu::loadMenu()
{
	//using this->_path and levelLoader.h to load the tiles
	this->_tileSet = loadMap(this->_path, this->_map);
}

/*
draws the menu(prints thw buttons and the map according to the camera)
INPUT:
	none
OUTPUT:
	none
*/
void GameMenu::drawMenuUpdate()
{	
	cameraUpdate(this->_x, 0, BLOCK_SIZE, BLOCK_SIZE, this->_camera, this->_cameraPosition);//moves the camera according to x
	
	//moves the buttons and titles position according to the _x position
	moveVectorButtons(this->_buttons, this->_x);
	moveVectorButtons(this->_titles, this->_x);

	drawMap(this->_tileSet, this->_map);//draws the map

	//using this->_buttons, this->_titles and helper.h to load and draw the buttons
	drawVectorButtons(this->_buttons);
	drawVectorButtons(this->_titles);
}

/*
d`tor of menu
INPUT:
	none
OUTPUT:
	none
*/
GameMenu::~GameMenu()
{
	//removes allocated memory in buttons
	deleteButtons(this->_buttons);
	deleteButtons(this->_titles);

	this->_x = MOVE_BUTTON_CONST;
	this->_cameraPosition[CAMERA_POS_X_INDEX] = 0;
	this->_cameraPosition[CAMERA_POS_Y_INDEX] = 0;
	cameraUpdate(this->_x, 0, BLOCK_SIZE, BLOCK_SIZE, this->_camera, this->_cameraPosition);//moves the camera according to x

	//delets camera memory
	delete this->_camera;

	//deletes map memory
	this->_map.clear();
	al_destroy_bitmap(this->_tileSet);
}

/*
handles the run of the game
INPUT:
	screen - source screen of the program
OUTPUT:
	returns the menu status - the next mode(PLAY, AI, SCOREBOARD, CREDITS) or EXIT
*/
int GameMenu::handleGameMenu(ALLEGRO_DISPLAY* display)
{
	//sets up parameters
	bool done = false;
	bool draw = true;

	//setting allegro variables 
	ALLEGRO_EVENT events;//contains single event 
	ALLEGRO_EVENT_QUEUE* eventQueue = al_create_event_queue();//contains a queue of events
	ALLEGRO_TIMER* menuTimer = al_create_timer(1.0 / MENU_FPS); //sets up the local game handle timer (60 FPS)
	ALLEGRO_KEYBOARD_STATE keyState; //contains keyboard event 

	al_register_event_source(eventQueue, al_get_keyboard_event_source());//sets up keyboard
	al_register_event_source(eventQueue, al_get_timer_event_source(menuTimer));//sets up timers 
	al_register_event_source(eventQueue, al_get_display_event_source(display));//sets up screen
	
	this->drawMenuUpdate();//draws the menu 
	al_flip_display();

	al_start_timer(menuTimer);//starts timer

	while (!done)//while the user didnt exit
	{
		//waits for events
		al_wait_for_event(eventQueue, &events);

		//if the user exit the program
		if (events.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			this->_menuStatus = EXIT;//exit from the game
			done = true;//current run ends
			break;//faster exit 
		}
		//else if timer event
		else if (events.type == ALLEGRO_EVENT_TIMER)
		{
			this->_x ++;//changes x value(screen move)
			this->handleEndLevel();//checks if needs to return x to level start

			//gets keyboard state
			al_get_keyboard_state(&keyState);
			done = this->handleKeyboard(&keyState);//handles keyboard input
			draw = true;//enables draw
		}
		
		if (draw == true)//if draw enabled
		{
			draw = false;//closes it down until next animation timer will turn it up
			this->drawMenuUpdate();//draws the menu 
			al_flip_display();
		}
	}

	//destorys allocated memory with allegro5 functions
	al_destroy_event_queue(eventQueue);
	al_destroy_timer(menuTimer);
	
	return this->_menuStatus;
}


/*
handles the input from the user
INPUT:
	ALLEGRO_KEYBOARD_STATE* keyState - pointer to the keyboard state
OUTPUT:
	returns true if the input mean end game run, false if else
*/
int GameMenu::handleKeyboard(ALLEGRO_KEYBOARD_STATE* keyState)
{
	//sets up colors
	ALLEGRO_COLOR COLOR_FONT_WHITE = al_map_rgb(255, 255, 255);
	ALLEGRO_COLOR COLOR_FONT_DARK_WHITE = al_map_rgb(100, 100, 100);
	ALLEGRO_COLOR COLOR_FONT_BLUE = al_map_rgb(44, 117, 255);
	ALLEGRO_COLOR COLOR_FONT_DARK_BLUE = al_map_rgb(22, 59, 127);
	
	static int count = 0;//counts presses in resume, won and lost screen, to set the current button
	static int active = false;//when false, dosnt color any of the buttons, when true draw the current button
	int done = false;//the return value

	//hold state of down and up
	static int downHold = RELEASE;
	static int upHold = RELEASE;
	
	//changes counter value while pressing up and down

	if (al_key_down(keyState, ALLEGRO_KEY_DOWN))
	{
		downHold = HOLD;//holding the key 
	}
	else if (!al_key_down(keyState, ALLEGRO_KEY_DOWN) && downHold == HOLD)//unpress and prev hold
	{
		downHold = RELEASE; //changes to release
		if (active == false)//active is working now
		{
			active = true;
		}
		else
		{
			count++;//change count
		}
	}

	if (al_key_down(keyState, ALLEGRO_KEY_UP))
	{
		upHold = HOLD;//holding the key 
	}
	else if (!al_key_down(keyState, ALLEGRO_KEY_UP) && upHold == HOLD)//unpress and prev hold
	{
		upHold = RELEASE;//changes to release
		if (active == false)//active is working now
		{
			active = true;
		}
		else
		{
			if (count == 0)//change count
			{
				count = this->_buttons.size() - 1;//prevent oppisite effect(down goes up)
			}
			else
			{
				count--;//change count
			}
		}		
	}

	if (active == true)//if coloring buttons true
	{
		//changes color of buttons according to input, chosen button is colored white
		for (int i = 0; i < this->_buttons.size(); i++)
		{
			if (i == abs(count) % this->_buttons.size())//if the current index equals to the chosen button index
			{
				this->_buttons[i]->changeColor(COLOR_FONT_WHITE, COLOR_FONT_DARK_WHITE);//changes its color to white
			}
			else
			{
				this->_buttons[i]->changeColor(COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE);//keeps color at default blue
			}
		}

		if (al_key_down(keyState, ALLEGRO_KEY_ENTER))//if the user prssed enter(pressed on a button)
		{
			if (this->_buttons[abs(count) % this->_buttons.size()]->getText() == "Play")
			{
				this->_menuStatus = PLAY;//sets mode as PLAY
				done = true;
			}
			else if (this->_buttons[abs(count) % this->_buttons.size()]->getText() == "AI")
			{
				this->_menuStatus = AI;//sets mode as AI
				done = true;
			}
			else if (this->_buttons[abs(count) % this->_buttons.size()]->getText() == "ScoreBoard")
			{
				this->_menuStatus = SCORE_BOARD;//sets mode as SCOREBOARD
				done = true;
			}
			else if (this->_buttons[abs(count) % this->_buttons.size()]->getText() == "Credits")
			{
				this->_menuStatus = CREDITS;//sets mode as CREDITS
				done = true;
			}
			else if (this->_buttons[abs(count) % this->_buttons.size()]->getText() == "Exit")
			{
				this->_menuStatus = EXIT;//sets mode as EXIT
				done = true;
			}

			count = 0;//resets static counter
			active = false;//resets active
		}
	}

	if (al_key_down(keyState, ALLEGRO_KEY_ESCAPE))//if the user pressed ESC
	{
		this->_menuStatus = EXIT;//sets mode as EXIT
		done = true;
	}

	return done;//returns game loop state value
}

/*
returns x to 304 if level ends
INPUT:
	none
OUTPUT:
	none
*/
void GameMenu::handleEndLevel()
{
	int keyX = floor(this->_x / BLOCK_SIZE), keyXb = floor((this->_x + BLOCK_SIZE - 1) / BLOCK_SIZE);
	for (int j = 0; j < SCREEN_HEIGHT / BLOCK_SIZE; j++)
	{
		if (this->_map[j][keyX] == BLOCK_WIN || this->_map[j][keyXb] == BLOCK_WIN)
		{
			this->_x = MOVE_BUTTON_CONST;
			break;
		}
	}
}

void GameMenu::playBonusGame()
{
	//To be continued...
}