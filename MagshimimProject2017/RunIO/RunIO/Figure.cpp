#include "Figure.h"

#pragma once

/*
c`tor of figure
INPUT:
	const char* path - path of bitmap file 
	float posX - position in x dimension 
	float posY - position in y dimension 
	float runSpeed - run speed value(x dimension) 
	float jumpSpeed - jump speed value(y dimension)		
OUTPUT:
	none
(bool this->_jumpInProcces - always be false becuase the figure is never jumping at creation)
*/
Figure::Figure(const char* path, float posX, float posY, float runSpeed, float jumpSpeed)
{
	this->_path = path;
	
	this->_posX = posX;
	this->_posY = posY;
	
	this->_pngX = 32;
	this->_pngY = 0;

	this->_velX = 0;
	this->_velY = 0;

	this->_runSpeed = runSpeed;
	this->_jumpSpeed = jumpSpeed;
	
	this->_anmType = REST;
	this->_faceDir = RIGHT;

	this->_jumpInProcces = false;//defalut value false
}

/*
d`tor of figure
INPUT:
	none
OUTPUT:
	none
delete player bitmap with allegro 5 function
*/
Figure::~Figure()
{
	al_destroy_bitmap(this->_playerBitmap);
}

/*
loads the figure bitmap
INPUT:
	none
OUTPUT:
	none
*/
void Figure::LoadFigure()
{
	//set up colors
	ALLEGRO_COLOR CYAN = al_map_rgb(0, 255, 255);
	
	this->_playerBitmap = al_load_bitmap(this->_path);//creates and loads the bitmap using the path and allegro 5 function
	al_convert_mask_to_alpha(this->_playerBitmap, CYAN);//"removes" the cyan background of the figure(int the file)
}

/*
draws the figure into the screen buffer
INPUT:
	none
OUTPUT:
	draws the figure into the allegro screen buffer
*/
void Figure::drawFigure()
{
	int flag = NULL;
	if (this->_faceDir == LEFT)
	{
		flag = ALLEGRO_FLIP_HORIZONTAL; //changes direction of draw if the figure is pointing to left
	}
	al_draw_bitmap_region(this->_playerBitmap, this->_pngX, this->_pngY, FIGURE_WIDTH, FIGURE_HEIGHT, this->_posX, this->_posY, flag);//draws the bitmap using the location in the file, position in he map and the flag
}

/*
set x velocity 
INPUT:
	int velX - velocity in x 
OUTPUT:
	none
*/
void Figure::setVelocityX(int velX)
{
	this->_velX = velX;
}

/*
set y velocity
INPUT:
	int velY - velocity in y
OUTPUT:
	none
*/
void Figure::setVelocityY(int velY)
{
	this->_velY = velY;
}

/*
set animation style(walk, rest, jump)
INPUT:
	int anmType - type of animation (see enum "animationType")
OUTPUT:
	none
*/
void Figure::setAnimationType(int anmType)
{
	this->_anmType = anmType;
}

/*
set direction of the figure(left or right)
INPUT:
	int faceDir - direction (see enum "faceDirection")
OUTPUT:
	none
*/
void Figure::setFaceDirection(int faceDir)
{
	this->_faceDir = faceDir;
}

/*
set if the figure is jumping or not(true or false)
INPUT:
	bool jumpInProcces - true if jumping, else false
OUTPUT:
	none
*/
void Figure::setJumpStatus(bool jumpInProcces)
{
	this->_jumpInProcces = jumpInProcces;
}

/*
get jump status(yes for jumping, else no)
INPUT:
	none
OUTPUT:
	value in this->_jumpInProcces
*/
bool Figure::getJumpStatus()
{
	return this->_jumpInProcces;
}

/*
get the run speed
INPUT:
	none
OUTPUT:
	value in this->_runSpeed
*/
int Figure::getRunSpeed()
{
	return this->_runSpeed;
}

/*
get the jump speed
INPUT:
	none
OUTPUT:
	value in this->_jumpSpeed
*/
int Figure::getJumpSpeed()
{
	return this->_jumpSpeed;
}

/*
get the animation type
INPUT:
	none
OUTPUT:
	value in this->_anmType
*/
int Figure::getAnimationType()
{
	return this->_anmType;
}

/*
get the x position
INPUT:
	none
OUTPUT:
	value in this->_posX
*/
float Figure::getPosX()
{
	return this->_posX;
}

/*
get the y position
INPUT:
	none
OUTPUT:
	value in this->_posY
*/
float Figure::getPosY()
{
	return this->_posY;
}

/*
sets the x position
INPUT:
	float x - new x position 
OUTPUT:
	none
*/
void Figure::setPosX(float x)
{
	this->_posX = x;
}

/*
sets the y position
INPUT:
	float y - new y position
OUTPUT:
	none
*/
void Figure::setPosY(float y)
{
	this->_posY = y;
}

/*
according to the animation status, sets up the bitmap location of the figure,
INPUT:
	none
OUTPUT:
	none
*/
void Figure::changeBitmap()
{
	if (this->_anmType == WALK)//if the character suppose to walk
	{
		if (this->_pngX == PNG_REST || this->_pngX == PNG_RUN3 || this->_pngX == PNG_JUMP)//resets his bitmap location to point on png_run1 
		{
			this->_pngX = PNG_RUN1;
		}
		else if (this->_pngX >= PNG_RUN1 && this->_pngX < PNG_RUN3)//if reset uneeded, adds 32 (MOVE_TO_NEXT_ANM) to his bitmap location
		{
			this->_pngX += MOVE_TO_NEXT_ANM;
		}
	}
	else if (this->_anmType == JUMP)//if the character suppose to jump
	{
		this->_pngX = PNG_JUMP;//bitmap location to point on png_jump
	}

	else if (this->_anmType == REST)//if the character suppose to rest
	{
		this->_pngX = PNG_REST;//bitmap location to point on png_rest
	}
}


/*RAZ DOCUMENTATION PART -> COMMENT NEEDED*/ 
int Figure::handleFigureMovement(vector<vector<int>> map)
{
	if (this->_jumpInProcces) //if still jumps, continues with gravity
	{
		this->_velY += GRAVITY;
		
	}
	else
	{
		this->_velY = 0;
	}
	this->_posX += this->_velX;
	if (this->handleDied(map)) //checks if died after change in x value
	{
		return 0;
	}
	else if (this->handleWin(map)) //checks if won after change in x value
	{
		return 2;
	}
	if (!this->handleCollision(map)) //checks if change causes to collision
	{
		this->_posX -= this->_velX;
	}
	this->_posY += this->_velY;
	if (this->handleDied(map)) //checks if died after change in y value
	{
		return 0;
	}
	else if (this->handleWin(map)) //checks if won after change in y value
	{
		return 2;
	}
	if (!this->handleCollision(map)) //checks if change causes to collision
	{
		//fixes collision
		if (this->_velY < 0)
		{
			this->fixJump(map);
		}
		else if (this->_velY > 0)
		{
			this->fixFall(map);
		}
		this->_velY = 0;
	}
	return 1;
}
void Figure::handleLanding(vector<vector<int>> map)
{
	int keyX = floor(this->_posY / 32), keyY = floor(this->_posX / 32), keyXb = floor((this->_posY + 32) / 32), keyYb = floor((this->_posX + 31) / 32);
	//checks if he's landing

	if (keyX >= map.size())
	{
		keyX = map.size() - 1;
	}
	if (keyY >= map[0].size())
	{
		keyY = map[0].size() - 1;
	}
	if (keyXb >= map.size())
	{
		keyXb = map.size() - 1;
	}
	if (keyYb >= map[0].size())
	{
		keyYb = map[0].size() - 1;
	}

	this->_jumpInProcces = !((map[keyXb][keyY] != BLOCK_AIR && map[keyXb][keyY] != BLOCK_BACKGND) || (map[keyXb][keyYb] != BLOCK_AIR && map[keyXb][keyYb] != BLOCK_BACKGND)) || (this->_velY < 0);
	if (this->_jumpInProcces == false)
	{
		this->_posY = keyX*32;
		this->_velX = 0;
		if (this->_anmType == JUMP)
		{
			this->_anmType = REST;
		}
		
	}
}
bool Figure::handleCollision(vector<vector<int>> map) //checks if there's collision
{
	int keyX = floor(this->_posY / 32), keyY = floor(this->_posX / 32), keyXb = floor((this->_posY + 31) / 32), keyYb = floor((this->_posX + 31) / 32);
	if (keyYb <= 0)
		return false;
	else if ((map[keyX][keyY] != BLOCK_AIR && map[keyX][keyY] != BLOCK_BACKGND) || (map[keyX][keyYb] != BLOCK_AIR && map[keyX][keyYb] != BLOCK_BACKGND))
	{
		return false;
	}
	else if ((map[keyXb][keyY] != BLOCK_AIR && map[keyXb][keyY] != BLOCK_BACKGND) || (map[keyXb][keyYb] != BLOCK_AIR && map[keyXb][keyYb] != BLOCK_BACKGND))
	{
		return false;
	}
	else
		return true;
}
void Figure::fixFall(vector<vector<int>> map) //fixes collision problems of falling
{
	this->_posY = floor(this->_posY / 32) * 32;
	int keyX = floor(this->_posX / 32);
	int keyXb = floor((this->_posX + 31) / 32);
	//if falling made it enter the block, the while loop helps the figure get out of the blocks
	while ((map[this->_posY / 32][keyX] != BLOCK_AIR && map[this->_posY / 32][keyX] != BLOCK_BACKGND) || (map[this->_posY / 32][keyXb] != BLOCK_AIR && map[this->_posY / 32][keyXb] != BLOCK_BACKGND))
	{
		this->_posY -= 32;
	}
}
void Figure::fixJump(vector<vector<int>> map) //fixes collision problems of jumping
{
	this->_posY = floor(this->_posY / 32) * 32;
	int keyX = floor(this->_posX / 32);
	int keyXb = floor((this->_posX + 31) / 32);
	//if falling made it enter the block, the while loop helps the figure get out of the blocks
	while ((map[this->_posY / 32][keyX] != BLOCK_AIR && map[this->_posY / 32][keyX] != BLOCK_BACKGND) || (map[this->_posY / 32][keyXb] != BLOCK_AIR && map[this->_posY / 32][keyXb] != BLOCK_BACKGND))
	{
		this->_posY += 32;
	}
}
//true - if died, false - if didn't die
bool Figure::handleDied(vector<vector<int>> map)
{
	int keyX = floor(this->_posY / 32), keyY = floor(this->_posX / 32), keyXb = floor((this->_posY + 31) / 32), keyYb = floor((this->_posX + 31) / 32);
	if (map[keyX][keyY] == BLOCK_FALL_TO_DEATH || map[keyX][keyYb] == BLOCK_FALL_TO_DEATH || map[keyXb][keyY] == BLOCK_FALL_TO_DEATH || map[keyXb][keyYb] == BLOCK_FALL_TO_DEATH)
	{
		return true;
	}
	return false;
}

//true - if won, false - if didn't win
bool Figure::handleWin(vector<vector<int>> map)
{
	int keyX = floor(this->_posY / 32), keyY = floor(this->_posX / 32), keyXb = floor((this->_posY + 31) / 32), keyYb = floor((this->_posX + 31) / 32);
	if (map[keyX][keyY] == BLOCK_WIN || map[keyX][keyYb] == BLOCK_WIN || map[keyXb][keyY] == BLOCK_WIN || map[keyXb][keyYb] == BLOCK_WIN)
	{
		return true;
	}
	return false;
}
