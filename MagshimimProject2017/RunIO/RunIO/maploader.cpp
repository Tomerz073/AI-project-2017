#include "maploader.h"

#pragma once


ALLEGRO_BITMAP* loadMinimap(const char* filename)
{
	ALLEGRO_BITMAP* tileSet = al_load_bitmap(filename);
	al_convert_mask_to_alpha(tileSet, al_map_rgb(0, 255, 255));
	return tileSet;
}

void drawMinimap(ALLEGRO_BITMAP* tileSet, string areaStr, int figureX)
{

	int distance = 0;
	if (figureX > MOVE_BUTTON_CONST) //CONSTANT
	{
		distance = figureX - MOVE_BUTTON_CONST;
	}
	int i = 0, j = 0, temp = 0;
	for (i = 0; i <= FIGURE_AREA_CONST * 2; i++)
	{
		for (j = 0; j <= FIGURE_AREA_CONST * 2; j++)
		{
			temp = areaStr[(i*(FIGURE_AREA_CONST * 2 + 1) + j) * 2];
			al_draw_tinted_bitmap_region(tileSet, al_map_rgba_f(1, 1, 1, 0.8), (temp - '0') * MINI_BLOCK_SIZE, 0, MINI_BLOCK_SIZE, MINI_BLOCK_SIZE, j * MINI_BLOCK_SIZE + 16 + distance, i * MINI_BLOCK_SIZE + 128, NULL);
		}
	}
}

/*
loads map from text file
INPUT:
const char* filename - filename of the map and tiles
vector<vector<int>> &map - referance to the empty map of the level
OUTPUT:
returns tileset
*/
ALLEGRO_BITMAP* loadMap(const char* filename, vector<vector<int>> &map)
{
	ALLEGRO_BITMAP* tileSet = NULL;

	int state = NULL;

	string line, value;
	int space;

	//opens textfile which contains map data
	std::ifstream openfile(filename);
	//checks if opened correctly
	if (openfile.is_open())
	{
		//continues while it's end of file
		while (!openfile.eof())
		{
			//gets line
			std::getline(openfile, line);
			//changes state of reading from textfile
			if (line.find("[tileset]") != string::npos)
			{
				state = state_tileset;
				continue;
			}
			else if (line.find("[map]") != string::npos)
			{
				state = state_map;
				continue;
			}

			switch (state)
			{
				//state tileset - loads the png of the map tiles
			case state_tileset:
			{
				if (line.length() > 0)
				{
					tileSet = al_load_bitmap(line.c_str());
					al_convert_mask_to_alpha(tileSet, al_map_rgb(0, 255, 255));
				}
				break;
			}
			//state map - gets the tilemap from textfile
			case state_map:
			{
				vector<int> tempVector;
				std::stringstream str(line);
				while (!str.eof())
				{
					std::getline(str, value, ' '); //gets line, space char is the divider
					if (value.length() > 0)
					{
						tempVector.push_back(atoi(value.c_str()));
					}
				}
				map.push_back(tempVector);
				break;
			}
			}
		}
		//closes file
		openfile.close();
	}
	else
	{
		cout << "Error lmao\n";
	}

	return tileSet;
}

/*
draws map according to tilemap and the png of the tiles
INPUT:
ALLEGRO_BITMAP* tileSet - the tileset
vector<vector<int>>& map - the map
OUTPUT:
draws the map into allegro screen buffer
*/
void drawMap(ALLEGRO_BITMAP* tileSet, vector<vector<int>>& map)
{
	int i = 0, j = 0;
	for (i = 0; i < map.size(); i++)
	{
		for (j = 0; j < map[i].size(); j++)
		{
			al_draw_bitmap_region(tileSet, map[i][j] * BLOCK_SIZE, 0, BLOCK_SIZE, BLOCK_SIZE, j * BLOCK_SIZE, i * BLOCK_SIZE, NULL);
		}
	}
}