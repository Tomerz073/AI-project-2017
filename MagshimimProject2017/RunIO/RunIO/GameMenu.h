#include "defines.h"

#define MENU_FPS 60

#pragma once

using namespace std;

class TextButton;

class GameMenu
{
private:

	int _x;

	const char* _path;
	int _menuStatus;

	vector<TextButton*> _buttons;
	vector<TextButton*> _titles;

	vector<vector<int>> _map;
	ALLEGRO_BITMAP* _tileSet;

	//camera - optional
	float _cameraPosition[CAMERA_ARR_MAX];
	ALLEGRO_TRANSFORM* _camera;

public:
	GameMenu(const char* path);
	~GameMenu();
	void loadMenu();
	void drawMenuUpdate();
	int handleGameMenu(ALLEGRO_DISPLAY* display);
	int handleKeyboard(ALLEGRO_KEYBOARD_STATE* keyState);
	void loadButtons();
	void handleEndLevel();
	void playBonusGame();//for raz's school project
};