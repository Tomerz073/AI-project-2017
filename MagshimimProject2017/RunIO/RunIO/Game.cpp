#include "Game.h"


#pragma once

/*
the c`tor of game
INPUT:
none
OUTPUT:
none
put default values in score(0) and gamestatus(play), creates new figure level and leveltimer with their default values
*/
Game::Game()
{
	this->_score = 0;
	this->_screenStatus = PLAY;//_screenStatus could be only PLAY, MENU -> return to menu, or EXIT -> exit the game 

	this->_figure = new Figure(FIGURE_PATH, FIGURE_X_POS, FIGURE_Y_POS, RUN_SPEED, JUMP_SPEED);
	this->_level = new GameLevel(LEVEL_PATH);
	this->_timerLevel = new TimerLevel();
}

/*
loads the game - load figure and level
INPUT:
none
OUTPUT:
none
*/
void Game::loadGame(string mapPath)
{
	this->_figure->LoadFigure();//loads figure
	this->_level->loadLevel(mapPath);//loads level

}

/*
deletes allocated memory on figure, level and leveltimer
INPUT:
none
OUTPUT:
none
*/
Game::~Game()
{
	delete this->_figure;
	delete this->_level;
	delete this->_timerLevel;
}

/*
handles the run of the game
INPUT:
screen - source screen of the program
OUTPUT:
handles game, returns EXIT if the user chose to return to menu, PLAY if the user chose to play the game again
*/
int Game::handleGame(ALLEGRO_DISPLAY* display)
{
	//this->_minimapTileset = loadMinimap(MINIMAP_PATH);


	int done = false;//use for exit from the run of the current game
	int draw = true;//let the program to draw the current perimitives

	//setting allegro variables 
	ALLEGRO_EVENT events;//contains single event 
	ALLEGRO_EVENT_QUEUE* eventQueue = al_create_event_queue();//contains a queue of events
	ALLEGRO_TIMER* countdownTimer = al_create_timer(1.0 / COUNTDOWN_FPS); //sets up timer for timerLevel countdown (1 FPS)
	ALLEGRO_TIMER* gameTimer = al_create_timer(1.0 / GAME_FPS); //sets up the local game handle timer (60 FPS)
	ALLEGRO_TIMER* animationTimer = al_create_timer(1.0 / ANM_FPS); //sets up the animation timer (15 FPS)
	ALLEGRO_KEYBOARD_STATE keyState; //contains keyboard event 

	//sets up keyboard
	al_register_event_source(eventQueue, al_get_keyboard_event_source());

	//sets up timers 
	al_register_event_source(eventQueue, al_get_timer_event_source(gameTimer));
	al_register_event_source(eventQueue, al_get_timer_event_source(countdownTimer));
	al_register_event_source(eventQueue, al_get_timer_event_source(animationTimer));

	//sets up screen
	al_register_event_source(eventQueue, al_get_display_event_source(display));

	//draws the game
	this->drawGameUpdate();
	al_flip_display();

	//starts timers
	al_start_timer(countdownTimer);
	al_start_timer(animationTimer);
	al_start_timer(gameTimer);

	while (!done)//while the user didnt exit
	{
		al_wait_for_event(eventQueue, &events); //gets current events

		//if the user exit the program
		if (events.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			this->_screenStatus = EXIT;//exit from the game
			done = true;//current run ends
			break;//faster exit 
		}
		//else if timer event
		else if (events.type == ALLEGRO_EVENT_TIMER)
		{
			//gets keyboard state
			al_get_keyboard_state(&keyState);

			if (events.timer.source == countdownTimer && this->_level->getLevelState() == GAMEPLAY && done != true)//if the timer is countdownTimer and the user is playing
			{
				//decreses timerlevel by 1.0 / COUNTDOWN_FPS second
				this->_timerLevel->decreaseTimer(1.0 / COUNTDOWN_FPS);

				if (this->_timerLevel->getTimeLeft() == TIMER_END_NUM)//if time runs out
				{
					this->_level->setLevelState(LOST);//change to lose mode
				}
			}

			if (events.timer.source == gameTimer)//if the timer is game timer
			{
				done = this->handleKeyboard(&keyState);//handles keyboard input

				if (this->_level->getLevelState() == GAMEPLAY && done != true)// if the user is playing
				{
					this->handleFigureCollision();//handles the collison of the figure
					this->updateScore();//updates the score
				}

				draw = true;//enables draw
			}

			if (events.timer.source == animationTimer)//if the timer is animation timer, handles drawing
			{
				this->_figure->changeBitmap();//changes the animation style(bitmap file location) of the figure
			}
		}

		if (draw == true && done != true)//id draw enabled
		{
			draw = false;//closes it down until next animation timer will turn it up
			this->drawGameUpdate();//draws the game
			al_flip_display();
		}
	}

	//destroys allocated memory with allegro 5 functions
	al_destroy_event_queue(eventQueue);
	al_destroy_timer(gameTimer);
	al_destroy_timer(countdownTimer);
	al_destroy_timer(animationTimer);

	return this->_screenStatus;//returns game status(EXIT, MENU or PLAY)
}

/*
handles the input from the user
INPUT:
ALLEGRO_KEYBOARD_STATE* keyState - pointer to the keyboard state
OUTPUT:
returns true if the input mean end game run, false if else
*/
int Game::handleKeyboard(ALLEGRO_KEYBOARD_STATE* keyState)
{
	//setting up allegro colors
	ALLEGRO_COLOR BLUE = al_map_rgb(44, 117, 255);
	ALLEGRO_COLOR DARK_BLUE = al_map_rgb(22, 59, 127);
	ALLEGRO_COLOR WHITE = al_map_rgb(255, 255, 255);
	ALLEGRO_COLOR GREY = al_map_rgb(100, 100, 100);

	static int count = 0;//counts presses in resume, won and lost screen, to set the current button that we want to activate
	static int activeColoring = false;//when false, dosnt color any of the buttons, when true draw the current button
	int done = false;//the return value

	//hold state of down and up
	static int downHold = RELEASE;
	static int upHold = RELEASE;
	static int resetHold = RELEASE;

	if (this->_level->getLevelState() == GAMEPLAY)//if the user is playing
	{
		if (al_key_down(keyState, ALLEGRO_KEY_RIGHT))//if the user pressed right 
		{
			this->_figure->setVelocityX(this->_figure->getRunSpeed());//set run speed
			if (!this->_figure->getJumpStatus())//if the user isnt jumping
			{
				this->_figure->setAnimationType(WALK);//sets animation as walk
			}
			this->_figure->setFaceDirection(RIGHT);//sets direction right
		}

		if (al_key_down(keyState, ALLEGRO_KEY_LEFT))//if the user pressed left
		{
			this->_figure->setVelocityX(-this->_figure->getRunSpeed());//set run speed
			if (!this->_figure->getJumpStatus())//if the user isnt jumping
			{
				this->_figure->setAnimationType(WALK);//sets animation as walk
			}
			this->_figure->setFaceDirection(LEFT);//sets direction right
		}

		if (al_key_down(keyState, ALLEGRO_KEY_UP) && !this->_figure->getJumpStatus())//if the user pressed up and not jumping
		{
			this->_figure->setVelocityY(-this->_figure->getJumpSpeed());//set jump speed
			this->_figure->setAnimationType(JUMP);//sets animation as jump
			this->_figure->setJumpStatus(true);//sets jumpstatus as true in figure
		}

		if (!al_key_down(keyState, ALLEGRO_KEY_RIGHT) &&
			!al_key_down(keyState, ALLEGRO_KEY_LEFT) &&
			!al_key_down(keyState, ALLEGRO_KEY_UP))//if the user isnt pressing the game buttons/dont do anything
		{
			if (this->_figure->getAnimationType() != REST && !this->_figure->getJumpStatus())//sets rest mode if the figure isnt jumping and comes from another animation style
			{
				this->_figure->setVelocityX(0);//reset speed
				this->_figure->setVelocityY(0);//reset speed
				this->_figure->setAnimationType(REST);//sets animation as rest
			}
		}

		if (al_key_down(keyState, ALLEGRO_KEY_R) && (resetHold == RELEASE))//if the user pressed R
		{
			resetHold = HOLD;
			this->_screenStatus = PLAY;//puts game status as PLAY
			this->_level->setLevelState(GAMEPLAY);
			done = true;//exit the game but the game returns play so it returns to handle game func
		}
		else if (!al_key_down(keyState, ALLEGRO_KEY_R) && resetHold == HOLD)//if releases R
		{
			resetHold = RELEASE;
		}

		if (al_key_down(keyState, ALLEGRO_KEY_ESCAPE))//if the user pressed ESC
		{
			//PAUSE MODE
			this->_level->setLevelState(PAUSE);//puts the level at pause state(user paused the game)
		}
	}

	else if (this->_level->getLevelState() == PAUSE ||
		this->_level->getLevelState() == LOST ||
		this->_level->getLevelState() == WON) //if the user paused the game, won or lost
	{

		//changes counter value while pressing up and down

		if (al_key_down(keyState, ALLEGRO_KEY_DOWN))
		{
			downHold = HOLD;//holding the key 
		}
		else if (!al_key_down(keyState, ALLEGRO_KEY_DOWN) && downHold == HOLD)//unpress and prev hold
		{
			downHold = RELEASE; //changes to release
			if (activeColoring == false)//active is working now
			{
				activeColoring = true;
			}
			else//if else counts
			{
				count++;//change count
			}
		}

		if (al_key_down(keyState, ALLEGRO_KEY_UP))
		{
			upHold = HOLD;//holding the key 
		}
		else if (!al_key_down(keyState, ALLEGRO_KEY_UP) && upHold == HOLD)//unpress and prev hold
		{
			upHold = RELEASE;//changes to release
			if (activeColoring == false)//active is working now
			{
				activeColoring = true;
			}
			else
			{
				if (count == 0)//change count
				{
					count = this->_level->getButtons().size() - 1;//prevent opposite effect(down goes up)
				}
				else
				{
					count--;//change count
				}
			}
		}

		if (activeColoring == true)//if coloring buttons true
		{
			//changes color of buttons according to input, chosen button is colored white
			for (int i = 0; i < this->_level->getButtons().size(); i++)
			{
				if (i == abs(count) % this->_level->getButtons().size())//if the current index equals to the chosen button index
				{
					this->_level->getButtons()[i]->changeColor(WHITE, GREY);//changes its color to white
				}
				else
				{
					this->_level->getButtons()[i]->changeColor(BLUE, DARK_BLUE);//keeps color at default blue
				}
			}

			if (al_key_down(keyState, ALLEGRO_KEY_ENTER) || al_key_down(keyState, ALLEGRO_KEY_LEFT))//if the user prssed enter(pressed on a button)
			{
				if (this->_level->getButtons()[abs(count) % this->_level->getButtons().size()]->getText() == "Return to Menu")//if user chose to return to menu
				{
					done = true;//ends game run 
					this->_screenStatus = MENU;//sets game status as menu(to return to menu)
				}
				else//if user chose to resume or to try again
				{
					this->_screenStatus = PLAY;//sets gametatus as PLAY
					this->_level->setLevelState(GAMEPLAY); //sets levelstate as gameplay

					if (this->_level->getButtons()[abs(count) % this->_level->getButtons().size()]->getText() == "Try again")//if user chose to try again
					{
						done = true;//exit the game but the game returns play so it returns to handle game func
					}
				}
				count = 0;//resets static counter
				activeColoring = false;//active turned off
			}
		}
	}
	return done;//return the state of game run
}

/*
handles the collision of the figure between different game blocks
INPUT:
none
OUTPUT:
none
*/
void Game::handleFigureCollision()
{
	int movementFlag = this->_figure->handleFigureMovement(this->_level->getMap()); //gets movement flag(MOVED, WON, DIED)

	this->_figure->handleLanding(this->_level->getMap());//handles figure handling with collisiions 

	if (movementFlag == FIGUREFLAG_DIED)//if the figure died due to collision 
	{
		this->_level->setLevelState(LOST);//change level state to lost
	}

	else if (movementFlag == FIGUREFLAG_MOVED_WON)//if the figure won due to collision 
	{
		//set up allegro colors
		ALLEGRO_COLOR YELLOW = al_map_rgb(248, 222, 0);
		ALLEGRO_COLOR ORANGE = al_map_rgb(255, 180, 0);

		//sets the score text
		string text;
		text = "SCORE:";
		text += to_string(this->_score);

		this->_level->setLevelState(WON);//changes level state to won

		//sets up the x and y positions of the score extbutton[in WON state level there are 2 titles, you won and score]
		int xPos = this->_level->getTitles()[TITLE_INDEX]->getXpos() + SCORE_X_POS_ADD;
		int yPos = this->_level->getTitles()[TITLE_INDEX]->getYpos() + SCORE_Y_POS_ADD;

		//pushes score button(text)
		this->_level->pushInTitles(new TextButton(xPos, yPos, YELLOW, ORANGE, ALLEGRO_ALIGN_LEFT, text.c_str(), FONT_PATH, SCORE_FONT_SIZE));

		if (this->addScore())
		{
			this->_level->pushInTitles(new TextButton(xPos, yPos + 32, YELLOW, ORANGE, ALLEGRO_ALIGN_LEFT, "Score has been added to scoreboard!", FONT_PATH, 12));
		}

	}
}

/*
the game draws the game update, the new game peremitives at their place, and the map with its camera
INPUT:
none
OUTPUT:
none
*/
void Game::drawGameUpdate()
{
	static int stateTransition = false;//sets static variable that draws the fade option and moves the buttons to only 1 time 

	if (this->_level->getLevelState() == GAMEPLAY)//if the user is playing
	{
		//updates camera position according to the values
		cameraUpdate(this->_figure->getPosX(), 0, BLOCK_SIZE, BLOCK_SIZE, this->_level->getCamera(), this->_level->getCameraPosition());

		//draws the figure, timer and level
		this->_level->drawLevel();
		this->_figure->drawFigure();
		this->_timerLevel->moveTimer(this->_figure->getPosX());
		this->_timerLevel->drawTimer();
		//drawMinimap(this->_minimapTileset, getFigureArea(this->_figure, this->_level->getMap()), this->_figure->getPosX());


		stateTransition = false;//for a potential stateTrinsition case
	}
	else //if the user isnt playing (pause/won/lost screen) 
	{
		//if there isnt screen transition in the drawing yet
		if (stateTransition == false)
		{
			//draws faded black background according to the camera position
			al_draw_filled_rectangle(
				this->_level->getCameraPosition()[CAMERA_POS_X_INDEX],
				this->_level->getCameraPosition()[CAMERA_POS_Y_INDEX],
				this->_level->getCameraPosition()[CAMERA_POS_X_INDEX] + SCREEN_WIDTH,
				this->_level->getCameraPosition()[CAMERA_POS_Y_INDEX] + SCREEN_HEIGHT,
				al_map_rgba(0, 0, 0, ALPHA_VAL));

			//moves the buttons and titles position according to the figure position
			moveVectorButtons(this->_level->getButtons(), this->_figure->getPosX());
			moveVectorButtons(this->_level->getTitles(), this->_figure->getPosX());

			stateTransition = true;//state transition done, no need to redo the things above
		}
		//draws the pause/won/lost menu buttons and titles
		this->_level->drawButtons();
	}
}

/*
updates the score of the game according to the timer and the position of the figure
INPUT:
none
OUTPUT:
none
*/
void Game::updateScore()
{
	this->_score = 1000 + this->_timerLevel->getTimeLeft() * MUL_TIME_LEFT;
}

/*
reset variables of figure and level
INPUT:
none
OUTPUT:
none
*/
void Game::resetGame()
{
	this->_figure->setPosX(FIGURE_X_POS);
	this->_figure->setPosY(FIGURE_Y_POS);
	this->_figure->setVelocityX(0);
	this->_figure->setVelocityY(0);
	this->_figure->setFaceDirection(RIGHT);
	this->_figure->setAnimationType(REST);
	this->_figure->changeBitmap();
	this->_score = 0;
	this->_level->setLevelState(GAMEPLAY);
	this->_figure->setJumpStatus(false);
	this->_timerLevel->resetColors();
	this->_timerLevel->setTimeLeft(TIME_LEFT);
}

bool Game::addScore()
{
	bool change = false;
	int scoresArr[3] = { 0 }, i = 0, temp = 0, newScore = this->_score;
	string line;
	ifstream highScores;
	highScores.open(TOP_SCORES_PATH);

	for (i = 0; i <3; i++)
	{
		getline(highScores, line);
		scoresArr[i] = stoi(line);
	}
	highScores.close();

	for (i = 0; i < 3; i++)
	{
		if (newScore > scoresArr[i])
		{
			change = true;
			temp = scoresArr[i];
			scoresArr[i] = newScore;
			newScore = temp;
		}
	}
	if (change)
	{
		ofstream highScoresO;
		highScoresO.open(TOP_SCORES_PATH);
		highScoresO << to_string(scoresArr[0]) + '\n';
		highScoresO << to_string(scoresArr[1]) + '\n';
		highScoresO << to_string(scoresArr[2]) + '\n';
		highScoresO.close();
	}


	return change; //lmao
}