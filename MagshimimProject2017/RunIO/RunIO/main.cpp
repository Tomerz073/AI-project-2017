#include "defines.h"
#include "GameMenu.h"
#include "GameLevel.h"
#include "Game.h"
#include "GameAI.h"
#include "GameHighScore.h"
#include "GameCredits.h"

#pragma once

using namespace std;

int main(int argc, char **argv)
{
	//default state is main menu
	int status = MENU;

	//initializes the classes
	GameMenu* gm = NULL;
	GameLevel* gl = NULL;
	Game* g = NULL;
	GameAI* gai = NULL;
	GameHighScore* ghs = NULL;
	GameCredits* gc = NULL;


	//initilazation of allegro5
	if (!al_init()) {
		fprintf(stderr, "failed to initialize allegro!\n");
		return -1;
	}

	//creating the screen in allegro5
	ALLEGRO_DISPLAY* display = al_create_display(SCREEN_WIDTH, SCREEN_HEIGHT);

	//if error, return -1
	if (!display) {
		cout << "failed to create display!";
		return -1;
	}

	//setting position and name
	al_set_window_position(display, 100, 100);
	al_set_window_title(display, "RunI/O");

	//clears screen - color black
	al_clear_to_color(al_map_rgb(0, 0, 0));

	//initilazation of allegro5 tools
	al_init_primitives_addon();
	al_install_keyboard();
	al_install_mouse();
	al_init_font_addon();
	al_init_ttf_addon();
	al_init_image_addon();

	//PROGRAM LOOP
	while (status != EXIT)
	{
		switch (status)
		{
		case MENU:
			//creates game menu
			gm = new GameMenu(LEVEL_PATH);//add new menu path for it
			gm->loadMenu();
			status = gm->handleGameMenu(display);
			delete gm;
			break;
		case PLAY:
			g = new Game();
			g->loadGame(LEVEL_PATH);
			status = g->handleGame(display);
			while (status == PLAY)
			{
				g->resetGame();
				status = g->handleGame(display);
			}
			delete g;
			break;
		case AI:
			gai = new GameAI();
			gai->loadGame(AI_PATH);
			gai->establishConnection();
			status = gai->handleGame(display);
			while (status == PLAY)
			{
				gai->resetGame();
				status = gai->handleGame(display);
			}
			delete gai;
			break;
		case SCORE_BOARD:
			ghs = new GameHighScore(HSCORE_PATH);
			ghs->loadHighScore();
			status = ghs->handleGameHighScore(display);
			delete ghs;
			break;
		case CREDITS:
			gc = new GameCredits(CREDIT_PATH);
			gc->loadCredits();
			status = gc->handleGameCredits(display);
			delete gc;
			break;
		}
	}

	al_destroy_display(display);//destorys the current display

	return 0;
}