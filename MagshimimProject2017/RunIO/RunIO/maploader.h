#include "defines.h"

#pragma once

enum LOAD_STATE { state_tileset, state_map };


ALLEGRO_BITMAP* loadMinimap(const char* filename);
void drawMinimap(ALLEGRO_BITMAP* tileSet, string areaStr, int figureX);

ALLEGRO_BITMAP* loadMap(const char* filename, vector<vector<int>> &map);
void drawMap(ALLEGRO_BITMAP* tileSet, vector<vector<int>>& map);