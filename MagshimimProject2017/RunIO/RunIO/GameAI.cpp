#undef UNICODE

#define WIN32_LEAN_AND_MEAN
#include <ws2tcpip.h>
#include <winsock2.h>
#include <stdio.h>
#include <Shellapi.h>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#include "GameAI.h"

#pragma once

/*
c`tor of GameAI
INPUT:
	none
OUTPUT:
	none
*/
GameAI::GameAI() : Game()
{
	//sets allegro colors
	ALLEGRO_COLOR WHITE = al_map_rgb(255, 255, 255);
	ALLEGRO_COLOR GREY = al_map_rgb(100, 100, 100);
	
	//sets start time
	this->_begin = clock();
	this->_seconds = 0;
	this->_minutes = 0;
	this->_hours = 0;

	//sets up distance parameters
	this->_fitness = 0;
	this->_blockProg = 0;
	this->_maxFitness = 0;

	//updates distance parameters
	this->updateFitness();

	this->_data = "";
	this->_aiData = "";
	this->_listenSocket = INVALID_SOCKET;
	this->_clientSocket = INVALID_SOCKET;
	
	//pushes buttons
	this->_aiTexts.push_back(new TextButton(TITLE1_X, TITLE1_Y, WHITE, GREY, ALLEGRO_ALIGN_LEFT, "", FONT_PATH, AI_TITLE_SIZE));
	this->_aiTexts.push_back(new TextButton(TITLE2_X, TITLE2_Y, WHITE, GREY, ALLEGRO_ALIGN_LEFT, "", FONT_PATH, AI_TITLE_SIZE));
	this->_aiTexts.push_back(new TextButton(TITLE3_X, TITLE3_Y, WHITE, GREY, ALLEGRO_ALIGN_LEFT, "", FONT_PATH, AI_TITLE_SIZE));
	this->_aiTexts.push_back(new TextButton(UP_X, UP_Y, WHITE, GREY, ALLEGRO_ALIGN_LEFT, "w", KEYCAPS_PATH, KEYCAP_SIZE));
	this->_aiTexts.push_back(new TextButton(LEFT_X, LEFT_Y, WHITE, GREY, ALLEGRO_ALIGN_LEFT, "z", KEYCAPS_PATH, KEYCAP_SIZE));
	this->_aiTexts.push_back(new TextButton(RIGHT_X, RIGHT_Y, WHITE, GREY, ALLEGRO_ALIGN_LEFT, "x", KEYCAPS_PATH, KEYCAP_SIZE));
}

/*
d`tor of GameAI
INPUT:
	none
OUTPUT:
	none
*/
GameAI::~GameAI()
{
	deleteButtons(this->_aiTexts);//delets texts
}

/*
handles the run of the game
INPUT:
	screen - source screen of the program
OUTPUT:
	handles game, returns EXIT if the user closed the program, MENU if escape pressed, PLAY if the ai still running
*/
int GameAI::handleGame(ALLEGRO_DISPLAY* display)
{
	this->_minimapTileset = loadMinimap(MINIMAP_PATH);

	int done = false;//use for exit from the run of the current game
	int draw = true;//let the program to draw the current perimitives

	int last_pos = this->_figure->getPosX();//value that contains the last position of the figure 
	//ranges in order to check "stuck in circle" situations
	int max_range = this->_figure->getPosX() + RANGE_MUL*RUN_SPEED; 
	int min_range = this->_figure->getPosX() - RANGE_MUL*RUN_SPEED;

	//setting allegro variables 
	ALLEGRO_EVENT events;//contains single event 
	ALLEGRO_EVENT_QUEUE* eventQueue = al_create_event_queue();//contains a queue of events
	ALLEGRO_TIMER* countdownTimer = al_create_timer(1.0 / COUNTDOWN_FPS); //sets up timer for timerLevel countdown (1 FPS)
	ALLEGRO_TIMER* gameTimer = al_create_timer(1.0 / GAMEAI_FPS); //sets up the local game handle timer (60 FPS)
	ALLEGRO_TIMER* animationTimer = al_create_timer(1.0 / ANM_FPS); //sets up the animation timer (15 FPS)
	ALLEGRO_TIMER* serverTimer = al_create_timer(1.0 / SERVER_FPS); //sets up the server timer (15 FPS)
	ALLEGRO_TIMER* stuckTimer = al_create_timer(1.0 / STUCK_FPS); //sets up stuck timer(1 FPS)
	ALLEGRO_KEYBOARD_STATE keyState; //contains keyboard event 

	//sets up keyboard
	al_register_event_source(eventQueue, al_get_keyboard_event_source());

	//sets up timers 
	al_register_event_source(eventQueue, al_get_timer_event_source(gameTimer));
	al_register_event_source(eventQueue, al_get_timer_event_source(countdownTimer));
	al_register_event_source(eventQueue, al_get_timer_event_source(animationTimer));
	al_register_event_source(eventQueue, al_get_timer_event_source(serverTimer));
	al_register_event_source(eventQueue, al_get_timer_event_source(stuckTimer));

	//sets up screen
	al_register_event_source(eventQueue, al_get_display_event_source(display));

	this->drawGameUpdate();//draws the game
	al_flip_display();//flips allegro buffer

	//starts timers
	al_start_timer(stuckTimer);
	al_start_timer(serverTimer);
	al_start_timer(countdownTimer);
	al_start_timer(animationTimer);
	al_start_timer(gameTimer);

	while (!done)//while the user didnt exit
	{		
		//update running time
		this->updateTime();
		
		al_wait_for_event(eventQueue, &events); //gets current events

		//if the user exit the program or the ai program ends
		if (this->_aiData == "EXIT" || events.type == ALLEGRO_EVENT_DISPLAY_CLOSE || this->_isConnected == false)
		{
			this->endConnection();//ends connection with server
			done = true;//current run ends

			if (this->_aiData == "EXIT" || this->_isConnected == false)//if ai ends return to menu
			{
				this->_screenStatus = MENU;//exit from the game
			}
			else //if program shutdown
			{
				this->_screenStatus = EXIT;//exit from the game
			}

			break;
		}

		//else if timer event
		else if (events.type == ALLEGRO_EVENT_TIMER)
		{
			//gets keyboard state
			al_get_keyboard_state(&keyState);

			//if the timer is countdownTimer and game play no reset no done
			if (events.timer.source == countdownTimer && this->_level->getLevelState() == GAMEPLAY && done != true && this->_data != "RESET")
			{
				//decreses timerlevel by 1.0 / COUNTDOWN_FPS second
				this->_timerLevel->decreaseTimer(1.0 / COUNTDOWN_FPS);

				if (this->_timerLevel->getTimeLeft() == TIMER_END_NUM)//if time runs out
				{
					this->_data = "RESET";//data now is reset
					this->_screenStatus = PLAY;
					done = true;//exit current game run
				}
			}

			//if the timer is game timer and no done and no reset
			if (events.timer.source == gameTimer && done != true && this->_data != "RESET")
			{
				done = this->handleKeyboard(&keyState) || this->handleFigureCollision();//handles keyboard input

				if (this->_data != "RESET")//if ai running in current run with no reset
				{
					this->updateFitness();//updates fitness and block prog
					this->_data = getFigureArea(this->_figure, this->_level->getMap()) + to_string(this->_blockProg) + " " + to_string(this->_fitness);
				}

				draw = true;//enables draw 
			}

			//if the timer is animation timer and no done and reset
			if (events.timer.source == animationTimer && done != true && this->_data != "RESET")
			{
				this->_figure->changeBitmap();//changes the animation style(bitmap file location) of the figure
				//sets fitness, max fitness
				this->_aiTexts[INFO2]->setText(("FITNESS:" + to_string(this->_fitness) + " MAX FITNESS:" + to_string(this->_maxFitness)).c_str());
				//update info 3
				this->_aiTexts[INFO3]->setText(("RUN TIME: " + to_string(this->_hours) + "h " + to_string(this->_minutes) + "m " + to_string(this->_seconds) + "s").c_str());
			}

			//if timer is stuck timer and no done and no reset
			if (events.timer.source == stuckTimer && done != true && this->_data != "RESET")
			{
				//if there was a change in position
				if (last_pos != this->_figure->getPosX())
				{
					last_pos = this->_figure->getPosX();//updates last position
					
					//updates range if passed min or max
					if (last_pos >= max_range)
					{
						max_range = last_pos + RUN_SPEED * RANGE_MUL;
						min_range = last_pos - RUN_SPEED * RANGE_MUL;
						
					}

					else if (last_pos <= min_range)
					{
						min_range = last_pos - RUN_SPEED * RANGE_MUL;
						max_range = last_pos + RUN_SPEED * RANGE_MUL;
					}

					else
					{
						this->_data = "RESET";//data now is reset
						this->_screenStatus = PLAY;
						done = true;//exit current game run
					}
				}

				//if no change in position - stuck
				else if (last_pos == this->_figure->getPosX())
				{
					this->_data = "RESET";//data now is reset
					this->_screenStatus = PLAY;
					done = true;//exit current game run
				}
			}

			//if the timer is server timer, sends info to server
			if (events.timer.source == serverTimer)
			{
				this->sendData(this->_data);//sends the data to the ai

				if (this->_data == "RESET" && this->_isConnected != false)//if told ai to reset
				{
					//udates figure and gets new data
					this->updateFitness();
					this->getData();

					if (this->_isConnected != false)
					{
						//analyzes first data and sends it
						this->_data = getFigureArea(this->_figure, this->_level->getMap()) + to_string(this->_blockProg) + " " + to_string(this->_fitness);
						this->sendData(this->_data);

						//update all infos
						this->_aiTexts[INFO1]->setText((this->_aiData).c_str());
						this->_aiTexts[INFO2]->setText(("FITNESS:" + to_string(this->_fitness) + " MAX FITNESS:" + to_string(this->_maxFitness)).c_str());
						this->_aiTexts[INFO3]->setText(("RUN TIME: " + to_string(this->_hours) + "h " + to_string(this->_minutes) + "m " + to_string(this->_seconds) + "s").c_str());
					}
					
				}

				this->_data = "";
			}
		}

		if (draw == true && done != true)//if draw enabled and no done
		{
			draw = false;//closes it down until next animation timer will turn it up
			this->drawGameUpdate();//draws the game
			al_flip_display();
		}
	}

	//destroys allocated memory with allegro 5 functions
	al_destroy_event_queue(eventQueue);
	al_destroy_timer(gameTimer);
	al_destroy_timer(countdownTimer);
	al_destroy_timer(animationTimer);
	al_destroy_timer(serverTimer);
	al_destroy_timer(stuckTimer);

	return this->_screenStatus;//returns game status(EXIT or PLAY or MENU)
}

/*
handles the input from the ai 
INPUT:
	ALLEGRO_KEYBOARD_STATE* keyState - pointer to the keyboard state
OUTPUT:
	returns true if the input mean end game run, false if else
*/
int GameAI::handleKeyboard(ALLEGRO_KEYBOARD_STATE* keyState)
{
	int done = false;

	static int escHold = RELEASE;

	//set up allegro colors
	ALLEGRO_COLOR GREEN = al_map_rgb(0, 255, 43);
	ALLEGRO_COLOR WHITE = al_map_rgb(255, 255, 255);
	ALLEGRO_COLOR GREY = al_map_rgb(100, 100, 100);

	if (this->_level->getLevelState() == GAMEPLAY)
	{
		if (al_key_down(keyState, ALLEGRO_KEY_PAD_6))//if the ai pressed right 
		{
			this->_figure->setVelocityX(this->_figure->getRunSpeed());//set run speed
			if (!this->_figure->getJumpStatus())//if the user isnt jumping
			{
				this->_figure->setAnimationType(WALK);//sets animation as walk
			}
			this->_figure->setFaceDirection(RIGHT);//sets direction right

			this->_aiTexts[RIGHT_KEY]->changeColor(GREEN, GREY);
		}
		else
		{
			this->_aiTexts[RIGHT_KEY]->changeColor(WHITE, GREY);
		}

		if (al_key_down(keyState, ALLEGRO_KEY_PAD_4))//if the ai pressed left
		{
			this->_figure->setVelocityX(-this->_figure->getRunSpeed());//set run speed
			if (!this->_figure->getJumpStatus())//if the user isnt jumping
			{
				this->_figure->setAnimationType(WALK);//sets animation as walk
			}
			this->_figure->setFaceDirection(LEFT);//sets direction right

			this->_aiTexts[LEFT_KEY]->changeColor(GREEN, GREY);
		}
		else
		{
			this->_aiTexts[LEFT_KEY]->changeColor(WHITE, GREY);
		}

		if (al_key_down(keyState, ALLEGRO_KEY_PAD_8) && !this->_figure->getJumpStatus())//if the ai pressed up and not jumping
		{
			this->_figure->setVelocityY(-this->_figure->getJumpSpeed());//set jump speed
			this->_figure->setAnimationType(JUMP);//sets animation as jump
			this->_figure->setJumpStatus(true);//sets jumpstatus as true in figure

			this->_aiTexts[UP_KEY]->changeColor(GREEN, GREY);
		}
		else if (this->_figure->getJumpStatus())
		{
			this->_aiTexts[UP_KEY]->changeColor(GREEN, GREY);
		}
		else
		{
			this->_aiTexts[UP_KEY]->changeColor(WHITE, GREY);
		}

		if (!al_key_down(keyState, ALLEGRO_KEY_PAD_6) &&
			!al_key_down(keyState, ALLEGRO_KEY_PAD_4) &&
			!al_key_down(keyState, ALLEGRO_KEY_PAD_8))//if the ai isnt pressing the game buttons/dont do anything
		{
			if (!this->_figure->getJumpStatus())//reset if figure isnt jumping
			{
				this->_data = "RESET";//data is now reset
				this->_screenStatus = PLAY;
				done = true;//exit current game run
			}
		}

		if (al_key_down(keyState, ALLEGRO_KEY_ESCAPE) && escHold == RELEASE)//if user pressed exit
		{
			escHold = HOLD;
		}
		else if (!al_key_down(keyState, ALLEGRO_KEY_ESCAPE) && escHold == HOLD)//if user released exit exit
		{
			escHold = RELEASE;
			this->_screenStatus = MENU;//exit from the game
			this->endConnection();//ends connection with server
			done = true;//game run ends 
		}
	}

	return done;
}

/*
handles the figure collision
INPUT:
	none
OUTPUT:
	returns true if the input mean end game run, false if else
*/
int GameAI::handleFigureCollision()
{
	int done = false;
	
	int movementFlag = this->_figure->handleFigureMovement(this->_level->getMap()); //gets movement flag(MOVED, WON, DIED)

	if (movementFlag == FIGUREFLAG_DIED || movementFlag == FIGUREFLAG_MOVED_WON)//if figure end 1 run
	{
		this->_data = "RESET";//data now reset
		this->_screenStatus = PLAY;
		done = true;
	}

	this->_figure->handleLanding(this->_level->getMap());//handles figure handling with collisiions [could be implemented in other way?]

	return done;
}

/*
crates connection with client
INPUT:
	none
OUTPUT:
	1 if fail 0 if success
*/
int GameAI::establishConnection()
{
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		this->_isConnected = false;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		this->_isConnected = false;
		return 1;
	}

	// Create a SOCKET for connecting to server
	this->_listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (this->_listenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		this->_isConnected = false;
		return 1;
	}

	// Setup the TCP listening socket
	iResult = ::bind(this->_listenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(this->_listenSocket);
		WSACleanup();
		this->_isConnected = false;
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(this->_listenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(this->_listenSocket);
		WSACleanup();
		this->_isConnected = false;
		return 1;
	}

	
	while (this->_clientSocket != NULL || this->_clientSocket != INVALID_SOCKET)
	{
		this->_clientSocket = accept(this->_listenSocket, NULL, NULL);
		if (this->_clientSocket == INVALID_SOCKET) {
			printf("accept failed with error: %d\n", WSAGetLastError());
			closesocket(this->_listenSocket);
			WSACleanup();
			this->_isConnected = false;
			return 1;
		}
		else
		{
			this->_isConnected = true;
			
			// No longer need server socket
			closesocket(this->_listenSocket);

			//gets data from ai before send
			this->getData();

			if (this->_isConnected != false)
			{
				//gets first data and sends it
				this->_data = getFigureArea(this->_figure, this->_level->getMap()) + to_string(this->_blockProg) + " " + to_string(this->_fitness);
				this->sendData(this->_data);

				if (this->_isConnected != false)
				{
					//update all infos
					this->_aiTexts[INFO1]->setText((this->_aiData).c_str());
					this->_aiTexts[INFO2]->setText(("FITNESS:" + to_string(this->_fitness) + " MAX FITNESS:" + to_string(this->_maxFitness)).c_str());
					this->_aiTexts[INFO3]->setText(("RUN TIME: " + to_string(this->_hours) + "h " + to_string(this->_minutes) + "m " + to_string(this->_seconds) + "s").c_str());

					break;
				}
			}
		}
	}

	return 0;
}

/*
sends data to ai
INPUT:
	string data
OUTPUT:
	1 if fail 0 if success
*/
int GameAI::sendData(string data)
{
	int iSendResult = send(this->_clientSocket, data.c_str(), strlen(data.c_str()), 0);
	if (iSendResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(this->_clientSocket);
		WSACleanup();
		this->_isConnected = false;
		return 1;
	}
	return 0;
}

/*
gets data to ai
INPUT:
	none
OUTPUT:
	1 or 0 if success, changes this->_aiData
*/
int GameAI::getData()
{
	char buffer[BUFFER_LEN];

	int iRecvResult = recv(this->_clientSocket, buffer, BUFFER_LEN, 0);
	if (iRecvResult == SOCKET_ERROR) {
		printf("recv failed with error: %d\n", WSAGetLastError());
		closesocket(this->_clientSocket);
		WSACleanup();
		this->_isConnected = false;
		return 1;
	}

	for (int i = 0; i < strlen(buffer); i++)
	{
		if (!(buffer[i] >= 'a' && buffer[i] <= 'z') && !(buffer[i] >= 'A' && buffer[i] <= 'Z') && !(buffer[i] >= '0' && buffer[i] <= '9') && buffer[i] != ' ' && buffer[i] != ':')
		{
			buffer[i] = '\0';
			break;
		}
	}

	this->_aiData = (string)buffer;

	return 0;
}

/*
ends connection with client
INPUT:
	none
OUTPUT:
	none
*/
void GameAI::endConnection()
{
	sendData("EXIT");
	WSACleanup();
}

/*
updates fitness, blockprog and max fitness according to figure position
INPUT:
	none
OUTPUT:
	none
*/
void GameAI::updateFitness()
{
	this->_fitness = this->_figure->getPosX();
	this->_blockProg = (this->_fitness % 32) / 32.0; //the amount of block the figure passed (0, 0.25, 0.5, 0.75)

	if (this->_fitness > this->_maxFitness)//sets max fitness
	{
		this->_maxFitness = this->_fitness;
	}
}

/*
update the runnning time
INPUT:
	none
OUTPUT:
	updates time
*/
void GameAI::updateTime()
{
	int elapsed_secs = double(clock() - this->_begin) / CLOCKS_PER_SEC;

	this->_hours = elapsed_secs / SEC_IN_H;
	this->_minutes = (elapsed_secs % SEC_IN_H) / SEC_IN_M;
	this->_seconds = (elapsed_secs % SEC_IN_H) % SEC_IN_M;

}

/*
draws the game
INPUT:
	none
OUTPUT:
    game drawing on screen
*/
void GameAI::drawGameUpdate()
{
	//updates camera position according to the values
	cameraUpdate(this->_figure->getPosX(), 0, BLOCK_SIZE, BLOCK_SIZE, this->_level->getCamera(), this->_level->getCameraPosition());

	//draws the figure, timer and level
	this->_level->drawLevel();
	this->_figure->drawFigure();
	this->_timerLevel->moveTimer(this->_figure->getPosX());
	moveVectorButtons(this->_aiTexts, this->_figure->getPosX());
	this->_timerLevel->drawTimer();
	drawVectorButtons(this->_aiTexts);
	//draws minimap
	drawMinimap(this->_minimapTileset, getFigureArea(this->_figure, this->_level->getMap()), this->_figure->getPosX());
}


