#include "GameLevel.h"
#include "Helper.h"
#include "Figure.h"

#pragma once

/*
c`tor of game level
INPUT:
	const char* path - the path of the gamelevel map that is being loaded
OUTPUT:
	none
puts levelstate as GAMEPLAY, creates the camera and loads the buttons
*/
GameLevel::GameLevel(const char* path)
{
	this->_path = path;
	this->_levelState = GAMEPLAY;
	this->_camera = new ALLEGRO_TRANSFORM();

	//sets up values in camera pos arr
	this->_cameraPosition[CAMERA_POS_X_INDEX] = 0;
	this->_cameraPosition[CAMERA_POS_Y_INDEX] = 0;

	this->loadButtons();//function that sets and loads the buttons
}

/*
loads buttons according to the state of the level at any state of game run
INPUT:
	none
OUTPUT:
	none
*/
void GameLevel::loadButtons()
{
	//setting up colors
	ALLEGRO_COLOR COLOR_FONT_BLUE = al_map_rgb(44, 117, 255);
	ALLEGRO_COLOR COLOR_FONT_DARK_BLUE = al_map_rgb(22, 59, 127);

	//removes allocated memory in buttons
	deleteButtons(this->_buttons);
	deleteButtons(this->_titles);

	if (this->_levelState == PAUSE)//if level state pause, adds resume and return to menu buttons and PUASE title
	{
		this->_buttons.push_back(new TextButton(GAME_BUTTON_X, GAME_BUTTON1_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Resume", FONT_PATH, BUTTON_SIZE));
		this->_buttons.push_back(new TextButton(GAME_BUTTON_X, GAME_BUTTON2_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Return to Menu", FONT_PATH, BUTTON_SIZE));
		this->_titles.push_back(new TextButton(PAUSE_TITLE_X, GAME_TITLE_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "PAUSE", FONT_PATH, TITLE_SIZE));
	}
	else if (this->_levelState == WON || this->_levelState == LOST)//if level state won or lost, adds try again and return to menu buttons and WON/LOST title
	{
		this->_buttons.push_back(new TextButton(GAME_BUTTON_X, GAME_BUTTON1_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Try again", FONT_PATH, BUTTON_SIZE));
		this->_buttons.push_back(new TextButton(GAME_BUTTON_X, GAME_BUTTON2_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "Return to Menu", FONT_PATH, BUTTON_SIZE));

		if (this->_levelState == WON)
		{
			this->_titles.push_back(new TextButton(WON_TITLE_X, GAME_TITLE_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "YOU WON!", FONT_PATH, TITLE_SIZE));
		}
		if (this->_levelState == LOST)
		{
			this->_titles.push_back(new TextButton(LOST_TITLE_X, GAME_TITLE_Y, COLOR_FONT_BLUE, COLOR_FONT_DARK_BLUE, ALLEGRO_ALIGN_LEFT, "YOU LOST! :(", FONT_PATH, TITLE_SIZE));
		}
	}
}

/*
loads the level map
INPUT:
	none
OUTPUT:
	none
*/
void GameLevel::loadLevel(string path)
{
	//using this->_path and levelLoader.h to load the tiles
	this->_tileSet =  loadMap(path.c_str(), this->_map);
}

/*
draws the level 
INPUT:
	none
OUTPUT:
	none
*/
void GameLevel::drawLevel()
{
	//using this->_map, this->_titles and maploader.h to load and draw the buttons
	drawMap(this->_tileSet, this->_map);
}

/*
draws the current buttons in the levels 
INPUT:
	none
OUTPUT:
	none
*/
void GameLevel::drawButtons()
{
	//using this->_buttons, this->_titles and helper.h to load and draw the buttons
	drawVectorButtons(this->_buttons);
	drawVectorButtons(this->_titles);	
}

/*
d`tor of gamelevel
INPUT:
	none
OUTPUT:
	none
*/
GameLevel::~GameLevel()
{
	//removes allocated memory in buttons
	deleteButtons(this->_buttons);
	deleteButtons(this->_titles);

	//delets camera memory
	delete this->_camera;

	//deletes map memory
	this->_map.clear();
	al_destroy_bitmap(this->_tileSet);
}

/*
gets cameraPosition
INPUT:
	none
OUTPUT:
	this->_cameraPosition
*/
float* GameLevel::getCameraPosition()
{
	return this->_cameraPosition;
}

/*
gets camera
INPUT:
	none
OUTPUT:
	this->_camera
*/
ALLEGRO_TRANSFORM* GameLevel::getCamera()
{
	return this->_camera;
}

/*
gets map
INPUT:
	none
OUTPUT:
	this->_map
*/
vector<vector<int>> GameLevel::getMap()
{
	return this->_map;
}

/*
gets levelState
INPUT:
	none
OUTPUT:
	this->_levelState
*/
int GameLevel::getLevelState()
{
	return this->_levelState;
}

/*
sets levelState and load the buttons
INPUT:
	int levelState - the new set value
OUTPUT:
	none
*/
void GameLevel::setLevelState(int levelState)
{
	this->_levelState = levelState;
	if (levelState == PAUSE || levelState == WON || levelState == LOST)
	{
		this->loadButtons();
	}
}

/*
gets buttons
INPUT:
	none
OUTPUT:
	this->_buttons 
*/
vector<TextButton*> GameLevel::getButtons()
{
	return this->_buttons;
}

/*
gets titles
INPUT:
	none
OUTPUT:
	this->_buttons
*/
vector<TextButton*> GameLevel::getTitles()
{
	return this->_titles;
}

/*
pushes single text button in titels
INPUT:
	none
OUTPUT:
	this->_buttons
*/
void GameLevel::pushInTitles(TextButton* tb)
{
	this->_titles.push_back(tb);
}

ALLEGRO_BITMAP* GameLevel::getTileSet()
{
	return this->_tileSet;
}