#include "defines.h"

#pragma once

using namespace std;

class TextButton;

class GameCredits
{
private:

	int _x;

	const char* _path;

	int _creditsStatus;

	vector<TextButton*> _buttons;
	vector<TextButton*> _titles;

	vector<vector<int>> _map;
	ALLEGRO_BITMAP* _tileSet;

public:
	GameCredits(const char* path);
	~GameCredits();
	void loadCredits();
	int handleGameCredits(ALLEGRO_DISPLAY* display);
	int handleKeyboard(ALLEGRO_KEYBOARD_STATE* keyState);
	void loadButtons();
};