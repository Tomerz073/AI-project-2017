#include "TextButton.h"

#pragma once

/*
sets up the button, incloding font, text and location
INPUT:
	int x - x location of the button 
	int y - y location of the button  
	ALLEGRO_COLOR mainColor - main color of the button 
	ALLEGRO_COLOR secondaryColor - secondary color of the button (for shadow effect) 
	int mode - allegro mode of the font
	const char* text - the text of the button
	const char* path - the path of the font 
	int size - the size of the font
OUTPUT:
	none
*/
TextButton::TextButton(int x, int y, ALLEGRO_COLOR mainColor, ALLEGRO_COLOR secondaryColor, int mode, const char* text, const char* path, int size)
{
	this->_font = al_load_font(path, size, NULL);
	this->_x = x;
	this->_y = y;
	this->_defX = x;
	this->_defY = y;
	this->_mainColor = mainColor;
	this->_secondaryColor = secondaryColor;
	this->_mode = mode;
	strcpy(this->_textNonConst, text);
	this->_text = this->_textNonConst;
}

/*
destroy allocated memory with allegro5 function
INPUT:
	none
OUTPUT:
	none
*/
TextButton::~TextButton()
{
	al_destroy_font(this->_font);//destory allegro font
}

/*
draws the button on the screen buffer
INPUT:
	none
OUTPUT:
	none
*/
void TextButton::drawButton()
{
	al_draw_text(this->_font, this->_secondaryColor, this->_x + 2, this->_y + 2, this->_mode, this->_text);//for shadow effect
	al_draw_text(this->_font, this->_mainColor, this->_x, this->_y, this->_mode, this->_text);//normal
}

/*
compares bettwen given set of colors and the current color
INPUT:
	ALLEGRO_COLOR newMainColor - main color 
	ALLEGRO_COLOR newSecondaryColor - secondary color for shadow effect
OUTPUT:
	true - equal, else false
*/
bool TextButton::equalColors(ALLEGRO_COLOR newMainColor, ALLEGRO_COLOR newSecondaryColor)
{
	if (this->_mainColor.a == newMainColor.a &&
		this->_mainColor.r == newMainColor.r &&
		this->_mainColor.g == newMainColor.g &&
		this->_mainColor.b == newMainColor.b &&

		this->_secondaryColor.a == newSecondaryColor.a &&
		this->_secondaryColor.r == newSecondaryColor.r &&
		this->_secondaryColor.g == newSecondaryColor.g &&
		this->_secondaryColor.b == newSecondaryColor.b)
	{
		return true;
	}

	return false;
}

/*
changes the color of the button
INPUT:
	ALLEGRO_COLOR newMainColor - main color 
	ALLEGRO_COLOR newSecondaryColor - secondary color for shadow effect
OUTPUT:
	none
*/
void TextButton::changeColor(ALLEGRO_COLOR newMainColor, ALLEGRO_COLOR newSecondaryColor)
{
	if (!this->equalColors(newMainColor, newSecondaryColor))//if colors not equal, change color
	{
		this->_mainColor = newMainColor;
		this->_secondaryColor = newSecondaryColor;
	}

}


/*
gets the text variable
INPUT:
	none
OUTPUT:
	this->_text as string
*/
string TextButton::getText()
{
	return string(this->_text, strnlen(this->_text, MAX_CHARS));
}

/*
moves the button according to a given x
INPUT:
	x_dis - x of figure/something else
OUTPUT:
	none
*/
void TextButton::moveButton(float posXchange)
{
	float distance = 0;
	if (posXchange > MOVE_BUTTON_CONST) //CONSTANT
	{
		distance = posXchange - MOVE_BUTTON_CONST;
	}

	this->_x = distance + this->_defX;
}

/*
sets the text of the button
INPUT:
	const char* text - the new text of the button
OUTPUT:
	none
*/
void TextButton::setText(const char* text)
{
	strcpy(this->_textNonConst, text);
	this->_text = this->_textNonConst;
}

/*
sets x position of the button
INPUT:
	int xPos - new x position
OUTPUT:
	none
*/
void TextButton::setXpos(int xPos)
{
	this->_x = xPos;
}

/*
sets y position of the button
INPUT:
	int xPos - new y position
OUTPUT:
	none
*/
void TextButton::setYpos(int yPos)
{
	this->_y = yPos;
}

/*
gets x position
INPUT:
	none
OUTPUT:
	this->_x
*/
int TextButton::getXpos()
{
	return this->_x;
}

/*
gets y position
INPUT:
	none
OUTPUT:
	this->_y
*/
int TextButton::getYpos()
{
	return this->_y;
}