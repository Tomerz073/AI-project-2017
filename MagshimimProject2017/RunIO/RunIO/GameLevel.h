#include "defines.h"
#include "maploader.h"
#include "TextButton.h"
#include "TimerLevel.h"

#define GAME_BUTTON_X 192
#define GAME_BUTTON1_Y 160
#define GAME_BUTTON2_Y 192
#define GAME_TITLE_Y 32
#define PAUSE_TITLE_X 208
#define WON_TITLE_X 190
#define LOST_TITLE_X 125

#pragma once

enum GAMELEVEL_STATE { GAMEPLAY, PAUSE, WON, LOST };

using namespace std;

class GameLevel
{
private:
	
	const char* _path;
	int _levelState;
	
	vector<TextButton*> _buttons;
	vector<TextButton*> _titles;

	vector<vector<int>> _map;
	ALLEGRO_BITMAP* _tileSet;

	float _cameraPosition[CAMERA_ARR_MAX];
	ALLEGRO_TRANSFORM* _camera;

public:
	GameLevel(const char* path);
	~GameLevel();
	void loadLevel(string path);
	void drawButtons();
	void drawLevel();
	void loadButtons();
	float* getCameraPosition();
	ALLEGRO_TRANSFORM* getCamera();
	vector<vector<int>> getMap();
	vector<TextButton*> getButtons();
	vector<TextButton*> getTitles();
	void pushInTitles(TextButton* tb);
	void setLevelState(int levelState);
	int getLevelState();
	ALLEGRO_BITMAP* getTileSet();
};


