#include "defines.h"

#pragma once

//defines for figure stats and physics
#define FIGURE_X_POS SCREEN_WIDTH / 2
#define FIGURE_Y_POS (SCREEN_HEIGHT - 96)
#define FIGURE_HEIGHT 32
#define FIGURE_WIDTH 32
#define RUN_SPEED 8
#define JUMP_SPEED 32
#define GRAVITY 4

//location in bitmap file
#define PNG_REST 32
#define PNG_RUN1 64
#define PNG_RUN2 96
#define PNG_RUN3 128
#define PNG_JUMP 160

//blocks to hanle
#define BLOCK_AIR 0
#define BLOCK_BACKGND 6
#define BLOCK_FALL_TO_DEATH 9
#define BLOCK_WIN 28

//define for the +32 command in changeBitmap, moves to next bitmapregion/png/animation type
#define MOVE_TO_NEXT_ANM 32

enum animationType { REST, WALK, JUMP };
enum faceDirection { RIGHT, LEFT };
enum figureMovementFlags { FIGUREFLAG_DIED, FIGUREFLAG_MOVED, FIGUREFLAG_MOVED_WON };

class Figure
{
private:

	const char* _path;
	ALLEGRO_BITMAP* _playerBitmap;
	
	int _pngX;
	int _pngY;

	float _posX;
	float _posY;
	
	float _velX;
	float _velY;
	
	int _runSpeed;
	int _jumpSpeed;
	
	int _anmType;
	int _faceDir;

	bool _jumpInProcces;

public:

	Figure(const char* path, float posX, float posY, float runSpeed, float jumpSpeed);
	~Figure();
	void LoadFigure();
	void drawFigure();
	
	void setVelocityX(int velX);
	void setVelocityY(int velY);
	void setAnimationType(int anmType);
	void setFaceDirection(int faceDir);
	void setJumpStatus(bool jumpInProcces);
	bool getJumpStatus();
	int getRunSpeed();
	int getJumpSpeed();
	int getAnimationType();
	float getPosX();
	float getPosY();
	void setPosX(float x);
	void setPosY(float y);

	void changeBitmap();
	int handleFigureMovement(vector<vector<int>> map);
	void handleLanding(vector<vector<int>> map);
	bool handleDied(vector<vector<int>> map);
	bool handleWin(vector<vector<int>> map);

	void fixJump(vector<vector<int>> map);
	void fixFall(vector<vector<int>> map);

	bool handleCollision(vector<vector<int>> map);
	bool handleCollisionY(vector<vector<int>> map);
};