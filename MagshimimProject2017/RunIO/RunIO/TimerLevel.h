#include "defines.h"
#include "TextButton.h"

#pragma once

using namespace std;

class TimerLevel
{
private:
	TextButton* _timerText;
	int _x;
	int _y;
	double _timeLeft;
public:
	TimerLevel();
	~TimerLevel();
	void decreaseTimer(double num);
	double getTimeLeft();
	void moveTimer(float posXchange);
	void drawTimer();
	void setTimeLeft(double time);
	void resetColors();
};