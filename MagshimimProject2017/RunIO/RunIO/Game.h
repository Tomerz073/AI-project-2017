#include "defines.h"
#include "Figure.h"
#include "GameLevel.h"
#include "TimerLevel.h"
#include "TextButton.h"
#include "maploader.h"
#include "Helper.h"

//defines for allegro timers
#define COUNTDOWN_FPS 1
#define GAME_FPS 60
#define ANM_FPS 15

// for game score
#define MUL_TIME_LEFT 255
#define MUL_FIGURE_X_POS 10

#pragma once

class Game
{
private:
	int _score;


protected:
	int _screenStatus;
	ALLEGRO_BITMAP* _minimapTileset;
	TimerLevel* _timerLevel;
	Figure* _figure;
	GameLevel* _level;



public:
	Game();
	~Game();
	virtual void loadGame(string mapPath);
	virtual int handleGame(ALLEGRO_DISPLAY* display);
	int handleKeyboard(ALLEGRO_KEYBOARD_STATE* keyState);
	void handleFigureCollision();
	virtual void drawGameUpdate();
	void updateScore();
	void resetGame();
	bool addScore();
};