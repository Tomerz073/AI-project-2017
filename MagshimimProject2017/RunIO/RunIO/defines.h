#define _CRT_NONSTDC_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE

#include <allegro5/allegro.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <sstream>
#include <math.h>
#include <array>
#include <stdio.h>

#pragma once

using std::string;
using std::vector;
using std::cout;
using std::endl;

#define MAX_CHARS 255
#define MOVE_BUTTON_CONST 304

//defines for blocks and screen
#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define SCREEN_X_POS 100
#define SCREEN_Y_POS 100

//defines for timerlevel
#define TIMER_X_POS 600
#define TIMER_Y_POS 20
#define TIME_LEFT 60
#define TIMER_FONT 18
#define WARNING_NUM 10
#define TIMER_END_NUM -1

//defines for blocks
#define BLOCK_SIZE 32
#define MINI_BLOCK_SIZE 16

//defines for buttons
#define TITLE_SIZE 32
#define BUTTON_SIZE 14
#define MENU_BUTTON_SIZE 18
#define GAME_BUTTON_X 192
#define GAME_BUTTON1_Y 160
#define GAME_BUTTON2_Y 192
#define GAME_TITLE_Y 32
#define MENU_TITLE_Y 32
#define MENU_TITLE_X 208
#define PAUSE_TITLE_X 208
#define WON_TITLE_X 190
#define LOST_TITLE_X 125
#define MENU_BUTTON_X 112
#define MENU_BUTTON1_Y 128
#define MENU_BUTTON2_Y 176
#define MENU_BUTTON3_Y 224
#define MENU_BUTTON4_Y 272
#define MENU_BUTTON5_Y 320



//defines for allegro timers
#define COUNTDOWN_FPS 1
#define FPS 60
#define ANM_FPS 15

//defines for paths
#define FONT_PATH "Resources/PressStart2P.ttf"
#define FIGURE_PATH "Resources/character_main.png"
#define AI_PATH "Resources/ai_blockmap.txt"
#define LEVEL_PATH "Resources/level_blockmap2.txt"
#define MENU_PATH "Resources/menu_blockmap.txt"
#define HSCORE_PATH "Resources/hscore_blockmap.txt"
#define CREDIT_PATH "Resources/credit_blockmap.txt"

#define MINIMAP_PATH "Resources/minimap_block.png"

#define TOP_SCORES_PATH "Resources/hscores.txt"

#define MAGSHIMIM_LOGO_PATH "Resources/MAGSHIMIM.png"

//title of level is always the first parameter in titles
#define TITLE_INDEX 0
#define SCORE_INDEX 1

//score button(text) defines
#define SCORE_X_POS_ADD  -10
#define SCORE_Y_POS_ADD  64
#define SCORE_FONT_SIZE 23

//camera
#define CAMERA_POS_X_INDEX 0
#define CAMERA_POS_Y_INDEX 1
#define CAMERA_ARR_MAX 2

//fade option
#define ALPHA_VAL 128

//for game score
#define MUL_TIME_LEFT 255
#define MUL_FIGURE_X_POS 10

//for game AI
#define FIGURE_AREA_CONST 3

//useful enums
enum SCREEN { EXIT, PLAY, AI, SCORE_BOARD, CREDITS, MENU };
enum BUTTON_HOLD_STATE { NONE, HOLD, RELEASE };





#define TIMER_SIZE 18

#define KEYCAPS_PATH "Resources/KeycapsRegular.ttf"