#include "Game.h"
#include <ctime>

//defines for connection
#define BUFFER_LEN 1024
#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

//defines for timers
#define STUCK_FPS 1
#define SERVER_FPS 60
#define GAMEAI_FPS 60

//defines for button vector
#define INFO1 0
#define INFO2 1
#define INFO3 2
#define UP_KEY 3
#define LEFT_KEY 4
#define RIGHT_KEY 5

//defines for range
#define RANGE_MUL 10

//defines for titles
#define LEFT_X 245
#define LEFT_Y 130
#define RIGHT_X 335
#define RIGHT_Y 130
#define UP_X 290
#define UP_Y 100
#define TITLE1_X 10
#define TITLE1_Y TIMER_Y_POS
#define TITLE2_X 10
#define TITLE2_Y TIMER_Y_POS+26
#define TITLE3_X 10
#define TITLE3_Y TIMER_Y_POS+52
#define AI_TITLE_SIZE 14
#define KEYCAP_SIZE 45

//defines for clock
#define SEC_IN_H 3600
#define SEC_IN_M 60

#pragma once

class GameAI : public Game
{
private:
	
	//times
	clock_t _begin;
	int _seconds;
	int _minutes;
	int _hours;

	//distance parameters
	int _fitness;
	int _maxFitness;
	float _blockProg;

	//string datas between c++ and python server and client
	string _data;
	string _aiData;

	//sockets
	SOCKET _listenSocket = INVALID_SOCKET;
	SOCKET _clientSocket = INVALID_SOCKET;

	//for valid connection
	bool _isConnected;
	
	//information about the ai
	vector<TextButton*> _aiTexts;

public:
	GameAI();
	~GameAI();
	int handleGame(ALLEGRO_DISPLAY* display);
	int establishConnection();
	void endConnection();
	int sendData(string data);
	int GameAI::getData();
	int handleKeyboard(ALLEGRO_KEYBOARD_STATE* keyState);
	int handleFigureCollision();
	void drawGameUpdate();
	void updateFitness();
	void updateTime();
	static void aiStartup();
};