#include "TextButton.h"
#include "Figure.h"

#pragma once

void drawVectorButtons(vector<TextButton*>& vtb);
void moveVectorButtons(vector<TextButton*>& vtb, float posXchange);
void deleteButtons(vector<TextButton*>& vtb);
void cameraUpdate(float x, float y, int width, int height, ALLEGRO_TRANSFORM* camera, float* cameraPosition);
string getFigureArea(Figure* figure, vector<vector<int>> map);