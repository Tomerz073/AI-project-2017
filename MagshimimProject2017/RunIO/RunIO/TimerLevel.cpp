#include "TimerLevel.h"

#pragma once

/*
c`tot of timerLevel
INPUT:
none
OUTPUT:
none
set up defult values
*/
TimerLevel::TimerLevel()
{
	ALLEGRO_COLOR WHITE = al_map_rgb(255, 255, 255);
	ALLEGRO_COLOR GREY = al_map_rgb(100, 100, 100);

	//default place in top right screen
	this->_x = TIMER_X_POS;
	this->_y = TIMER_Y_POS;
	//default time 30 seconds
	this->_timeLeft = TIME_LEFT;
	string timeLeft_str = to_string((int)this->_timeLeft);
	const char* timeLeft_char = timeLeft_str.c_str();
	//creates textbutton
	this->_timerText = new TextButton(this->_x, this->_y, WHITE, GREY, ALLEGRO_ALIGN_RIGHT, timeLeft_char, FONT_PATH, TIMER_SIZE);
}

/*
d`tor of timerlevel
INPUT:
none
OUTPUT:
none
*/
TimerLevel::~TimerLevel()
{
	delete this->_timerText;//deletes the textbutton
}

/*
decreses timer by 1
INPUT:
none
OUTPUT:
none
*/
void TimerLevel::decreaseTimer(double num) //decreases timer by amount of seconds
{
	//sets allegro colors
	ALLEGRO_COLOR RED = al_map_rgb(238, 24, 24);
	ALLEGRO_COLOR DARK_RED = al_map_rgb(148, 2, 2);
	ALLEGRO_COLOR WHITE = al_map_rgb(255, 255, 255);
	ALLEGRO_COLOR GREY = al_map_rgb(100, 100, 100);

	//sets the timerleft char string
	this->_timeLeft -= num;
	string timeLeft_str = to_string((int)this->_timeLeft);
	const char* timeLeft_char = timeLeft_str.c_str();
	this->_timerText->setText(timeLeft_char);

	if (this->_timeLeft == WARNING_NUM)//if time is 10 seconds
	{
		this->_timerText->changeColor(RED, DARK_RED);//changes color to red
	}

	else if (this->_timeLeft > WARNING_NUM  && this->_timerText->equalColors(RED, DARK_RED))//if not supposed to be red
	{
		this->_timerText->changeColor(WHITE, GREY);//changes color to white
	}
}

/*
draws the timer ofn the screen
INPUT:
none
OUTPUT:
none
*/
void TimerLevel::drawTimer()
{
	this->_timerText->drawButton();
}

/*
moves the timer according to a given x position
INPUT:
float posXchange - probably the x position of the figure
OUTPUT:
none
*/
void TimerLevel::moveTimer(float posXchange)
{
	this->_timerText->moveButton(posXchange);
}

/*
gets the time of the timer
INPUT:
none
OUTPUT:
this->_timeLeft
*/
double TimerLevel::getTimeLeft()
{
	return this->_timeLeft;
}

/*
sets time left
INPUT:
double time - the time to set
OUTPUT:
none
*/
void TimerLevel::setTimeLeft(double time)
{
	//sets the timerleft char string
	this->_timeLeft = time;
	string timeLeft_str = to_string((int)this->_timeLeft);
	const char* timeLeft_char = timeLeft_str.c_str();
	this->_timerText->setText(timeLeft_char);
}

//reset colors ti white
void TimerLevel::resetColors()
{
	ALLEGRO_COLOR WHITE = al_map_rgb(255, 255, 255);
	ALLEGRO_COLOR GREY = al_map_rgb(100, 100, 100);

	this->_timerText->changeColor(WHITE, GREY);
}

