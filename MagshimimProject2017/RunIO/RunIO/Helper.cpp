#include "Helper.h"

#define BLOCK_AIR 0
#define BLOCK_PHYSICAL 1
#define BLOCK_DEATH 0

#pragma once

/*
draws a given vector of textButtons
INPUT:
	vector<TextButton*> vtb - a given vector of text buttons
OUTPUT:
	none
*/
void drawVectorButtons(vector<TextButton*>& vtb)
{
	TextButton* temp;
	
	for (std::vector<TextButton*>::iterator it = vtb.begin(); it != vtb.end(); it++)
	{
		temp = *it;
		temp->drawButton();
	}
}

/*
deletes a given vector of textButtons
INPUT:
	vector<TextButton*> vtb - a given vector of text buttons
OUTPUT:
	none
*/
void deleteButtons(vector<TextButton*>& vtb)
{
	TextButton* temp;
	
	for (std::vector<TextButton*>::iterator it = vtb.begin(); it != vtb.end();)
	{
		temp = *it;
		it = vtb.erase(it);//erases from vector
		delete temp;//erases from memory
	}
}

/*
updates the comera location according to x,y (of the figure)
INPUT:
	float x - x of figure 
	float y - y is 0 
	int width - always 32 
	int height - always 32
	ALLEGRO_TRANSFORM* camera - the camera of the level  
	float* cameraPosition - the position of the given camera 
OUTPUT:
	none
*/
void cameraUpdate(float x, float y, int width, int height, ALLEGRO_TRANSFORM* camera, float* cameraPosition)
{
	//sets up the camera position according to the figure 
	cameraPosition[CAMERA_POS_X_INDEX] = -(SCREEN_WIDTH / 2) + (x + width / 2);
	cameraPosition[CAMERA_POS_Y_INDEX] = -(SCREEN_HEIGHT / 2) + (y + height / 2);

	//resets camera position if its located left to the 0,0 minimum 
	if (cameraPosition[CAMERA_POS_X_INDEX] < 0)
		cameraPosition[CAMERA_POS_X_INDEX] = 0;
	if (cameraPosition[CAMERA_POS_Y_INDEX] < 0)
		cameraPosition[CAMERA_POS_Y_INDEX] = 0;

	//transforms the camera
	al_identity_transform(camera);
	al_translate_transform(camera, -cameraPosition[CAMERA_POS_X_INDEX], -cameraPosition[CAMERA_POS_Y_INDEX]);
	al_use_transform(camera);
}

//returns vector of 9x9 area that surrounds the figure, vector[x][y] -> vector[x*13+y] //COMMENT NEEDED
string getFigureArea(Figure* figure,vector<vector<int>> map)
{
	string areaStr = "";
	int i = 0, j = 0;

	int keyX = floor(figure->getPosY() / 32), keyY = floor(figure->getPosX() / 32);

	for (i = keyX - FIGURE_AREA_CONST; i < keyX + FIGURE_AREA_CONST+1; i++)
	{
		for (j = keyY - FIGURE_AREA_CONST; j < keyY + FIGURE_AREA_CONST+1; j++)
		{
			if (i < 0 || j < 0 || i >= 16 || (std::to_string(map[i][j]) == "0" || std::to_string(map[i][j]) == "6" || std::to_string(map[i][j]) == "28")) //what is this 16 even meanS?
			{
				areaStr += (std::to_string(BLOCK_AIR) + " ");
			}
			else if (std::to_string(map[i][j]) == "9")
			{
				areaStr += (std::to_string(BLOCK_AIR) + " ");
			}
			else
			{
				areaStr += (std::to_string(BLOCK_PHYSICAL) + " ");
			}
		}
	}
	return areaStr;
}

/*
draws a given vector of textButtons
INPUT:
	vector<TextButton*> vtb - a given vector of text buttons
	float posXchange - the requierd change according to x pos
OUTPUT:
	none
*/
void moveVectorButtons(vector<TextButton*>& vtb, float posXchange)
{
	TextButton* temp;
	
	for (std::vector<TextButton*>::iterator it = vtb.begin(); it != vtb.end(); it++)
	{
		temp = *it;
		temp->moveButton(posXchange);
	}
}